cp ~/Documents/Research/references.bib ./references.bib
echo "Copied references"
chapters="000-Header.yaml ??-*md"
resizeres=1200x

pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--table-of-contents \
--columns=2 \
--metadata link-citations \
--variable urlcolor=cyan \
-o transfer-report.md $chapters #Incorporate all chapters into PDF file
echo "Stitched markdown documents together"

pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--table-of-contents \
--columns=2 \
--metadata link-citations \
--variable urlcolor=cyan \
--number-sections \
-o transfer-report.pdf $chapters #Incorporate all chapters into PDF file

echo "Finished PDF processing"

pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--mathjax \
--table-of-contents \
--metadata link-citations \
--variable urlcolor=cyan \
-o web/transfer-report.html $chapters #Incorporate all chapters into HTML file, for distribution online
echo "Finished HTML processing"
pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--mathjax \
--table-of-contents \
--metadata link-citations \
-o ./transfer-report.md $chapters #Incorporate all chapters into HTML file, for distribution online
echo "Finished Markdown processing"
echo "Converting Assets for web"
#cp -r assets/ web/assets/
mogrify -path web/ -resize $resizeres assets/*.png
echo "Writing comments to file 'writecomments.sh'"
bash writecomments.sh
echo "Done!"
