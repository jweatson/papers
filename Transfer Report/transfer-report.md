---
abstract: this is a test
author: Joseph Eatson
bibliography: './references.bib'
date: 'May-August 2018'
geometry: margin=1in
header: Transfer Report
link-citations: True
numbersections: True
title: Transfer Report
---

-   [Summary](#summary)
-   [Background](#background)
    -   [Stellar Winds](#stellar-winds)
        -   [Driving Forces](#driving-forces)
        -   [Seeking an Effective Model of Radiative Line
            Driving](#seeking-an-effective-model-of-radiative-line-driving)
    -   [Massive Binary Star Systems](#massive-binary-star-systems)
        -   [Wolf-Rayet Stars](#wolf-rayet-stars)
    -   [Computational Astrophysics](#computational-astrophysics)
        -   [Numerical Simulation](#numerical-simulation)
        -   [Radiative Transfer](#radiative-transfer)
-   [Literature Review](#literature-review)
    -   [Pinwheels in the Sky, With
        Dust](#pinwheels-in-the-sky-with-dust)
        -   [Research & Conclusions](#research-conclusions)
        -   [Comparison of Dust Mechanisms
            \label{dustcomphendrix}](#comparison-of-dust-mechanisms)
        -   [Overall Comparison With
            Project](#overall-comparison-with-project)
-   [Project Outline](#project-outline)
    -   [Towards a Comprehensive Dust
        Model](#towards-a-comprehensive-dust-model)
    -   [Simulation of Colliding Wind Binary
        Systems](#simulation-of-colliding-wind-binary-systems)
    -   [Comparison with Observational
        Data](#comparison-with-observational-data)
-   [Progress](#progress)
    -   [Dust Modelling](#dust-modelling)
        -   [Multiple Scalar Model](#multiple-scalar-model)
-   [Progression Beyond The First
    Year](#progression-beyond-the-first-year)
-   [Appendix](#appendix)
-   [References](#references)

Summary
=======

It is generally agreed upon that the bulk of dust formation in the
galaxy occurs inside star systems containing Asymptotic Giant Branch
(AGB) stars. However, there are additional formation processes that
produce a non-insignificant amount of interstellar dust. One such
process is dust production in Wind Collision Regions (WCR’s).

Wind Collision Regions form when the winds of two massive stars in a
binary pair collide. The resultant shock heats the surrounding
environment to temperatures exceeding $10^8 \text K$. An area of
particular interest is where the winds collide head on, producing the
most energetic conditions in the system, however this area is extremely
compact, occurring over an area smaller than $1 \text{AU}$, this
compactness, and the relative distance of nearest CWB systems excludes
direct observation, meaning that numerical hydrodynamic simulation must
be used.

The goal of this project is to simulate the conditions inside a CWB
system, in particular the dust properties of the system; this transfer
report will detail the physical phenomena occurring within the system,
an analysis of contemporary research into these systems, the current
state of the project as of the first year, and future work, both
immediate and long-term.

Background
==========

Stellar Winds
-------------

Since the turn of the century, stellar winds have been observed by
astronomers, and since the discovery of emission lines, spectroscopic
data has been obtained for them. Doppler shifts of over 100 kilometres
per second were viewed with scientific curiosity, and early photographic
observations noted the slowly increasing angular diameter of the clouds
- the idea of the universe as static and unchanging was being rapidly
considered.

By the 1940’s, early mathematical models of these systems were being
considered, however Underhill noted that radiation pressure alone was
not sufficient to account for the sheer amount of material being ejected
from OB stars - something was afoot, just out of sight.

In the second half of the century telescopes capable of seeing beyond
visible light were built, opening up new avenues of observation; these
new observatories showed us the infrared excesses of dust emission, and
the strong X-ray emission of interstellar shocks - this comprehensive
view of interstellar space allowed for the answering of long-standing
questions.

### Driving Forces

The primary radiative driving mechanism of low-mass stars is Thompson
scattering, a form of elastic scattering where the photon interacts with
a charged particle, changing the trajectory and velocity of the
particle. This interaction is entirely dependent on the Thomson cross
section, $\sigma_t$, which helps determine the likelihood of a collision
between a photon and a charged particle; in the case of an electron

$$
\sigma_t = \frac{8\pi}{3} \left( \frac{\alpha \hbar}{mc}\right)^2 \approx 6.652458 \times 10^{-29} \text m^2 ~~,
$$
<!-- perhaps include another equation to show the interaction probability of Thomson scattering -->

where $\alpha$ is the fine structure constant, $1/137$. The cross
section of an electron is extremely small, and even in the comparatively
dense regime of a stellar wind, the opacity is not sufficient to drive
fast, dense winds. For AGB stars, their low surface gravity results in
the driving of extremely dense but slow winds. But for massive stars,
capable of driving away the same amount of material but at speeds
exceeding $1000 \text{ km/s}$, there has to be a mechanism with a
significantly greater opacity in order to drive the wind at the observed
velocities and loss rates, while counteracting the significantly
stronger surface gravity.

  Star              $v_\infty~(\text{km s}^{-1})$   $\dot M~(\text{M}_\odot~\text{yr}^{-1})$
  ----------------- ------------------------------- ------------------------------------------
  Solar mass star   $400-700$                       $10^{-14}$
  AGB star          $10-30$                         $10^{-5}$
  OB star           1000-3000                       $10^{-6} - 10^{-5}$

<!-- Make this table more elaborate, add additional details, and wolf rayet stars to it -->
Instead, massive stars utilise radiative line driving as the motive
force behind their impressive winds. Hot stars emit large amounts of UV
radiation at a variety of frequencies, some of which correspond with
resonance lines of the elements in the wind; these resonance lines have
an opacity $10^6$ times greater than Thomson scattering
[@lamers_introduction_1999]; the rest of the wind is then accelerated
with the ions via *Coulomb coupling*. Due to the limited number of
resonant lines the motive force would be weaker than Thomson scattering
if not for Doppler shift; as the wind velocity increases the photons are
Doppler shifted in the reference frame of the atoms in the wind,
resulting in a greater number of absorption events, and a greater motive
force. The result of this phenomenon is increasing acceleration as the
wind speeds increase (up to its terminal velocity), resulting in
increased acceleration as the wind travels further from the star - the
inverse of the Thomson scattering acceleration curve.

<!-- Perhaps find a graph supporting this? -->
The *mg* hydrodynamics code used in this project supports radiative
driving, in addition to its default of using instantaneous acceleration.
As the acceleration of the wind is gradual, reaching a terminal velocity
at some distance from the star. For a binary system with a small
separation distance this can result in a significant deviation in the
structure of the collision; this is compounded by the effect of sudden
radiative braking and stagnation-point flow, where radiative line
driving from the secondary star acts on the flow from the primary star,
rapidly braking it - these effects are further detailed in
[@stevens_stagnation-point_1994] and [@gayley_sudden_1997].

<!-- verify this -->
### Seeking an Effective Model of Radiative Line Driving

As previously discussed, line driving is the mechanism of choice for
acceleration of dense, fast winds around massive stars; however, a
highly accurate simulation capable of modelling every single line,
including its scattering potential, and the path it takes through the
wind , would fall under the purview of ray-tracing for every single time
step of a simulation, something that is not computationally feasible.

Parameterising the equations reduces computational complexity, and
allows this to be calculated at every step in a hydrodynamical
simulation effectively; however, careful consideration must be made
regarding phenomena, in order for it to be effectively modelled. This is
also a good example of a core tenant of effective numerical simulation,
the trade-off between physical accuracy and simulation complexity.

<!--  This is very much a placeholder paragraph, especially regarding parameterisation -->
#### The CAK Formalism

The CAK formalism on the other hand seeks to parametrise the force
exerted on stellar wind material as a whole,

With these models, there exist critical points where line driving is
maximised due to

This formalism forms the basis behind all radiatively driven wind models
for massive stars,

#### The Sobolev Approximation

#### The Finite Disk Correction Factor

A final modification to the CAK formalism is considering the effect of
proximity to the star has on the photon flux received by the stellar
material, if the star is assumed to be a uniformly radiating surface,
then the photon flux decreases at close proximity.

At a distance however, assuming that each photon performs a single
scattering event, there are more available photons, leading to an
increase in flux - and an increase in force; combined with the decreased
force due to gravity from the star, this leads to a significant increase
in final velocity, explaining discrepancies from the earlier model.

The finite disk correction factor is defined as

$$
f = \frac{(1+\sigma)^{1+\alpha} - (1+\sigma \mu_c^2)^{1+\alpha}}
{(1+\alpha)(1-\mu_c^2)\sigma(1+\sigma)^\alpha}~,
$$

where $\sigma = d\ln(v/d)\ln(r-1)$, $\mu^2_c = 1-(R^2/r^2)$ and $\alpha$
is the CAK parameter characterising the distribution of optically thick
optically thin lines [@friend_theory_1986].

Massive Binary Star Systems
---------------------------

### Wolf-Rayet Stars

Wolf-Rayet stars, discovered by Charles Wolf and Georges Rayet in 1867,
are characterised by their extremely broad emission lines; the
classification can be further subdivided into WN, WC and WO subtypes
through the presence of a strong secondary nitrogen, carbon or oxygen
line.

The reason for these strong emission lines is due to the

Computational Astrophysics
--------------------------

Due to the small scale of the system and the comparative distance of CWB
systems, most analysis of these systems is done using simulations, which
are then compared to the scarce available observational data. Typically
the wind structure of the system is performed with [numerical
simulation](#numerical-simulation) and the fluid
<!-- is this a good word for it? --> properties of the system are used
to derive observable properties using [radiative
transfer](#radiative-transfer)

### Numerical Simulation

Inviscid, compressible flows, such as the movement of atoms and
molecules through the interstellar medium are governed by the Euler
equations, which in vector form are

$$
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
$$

$$
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
$$

$$
\frac{\partial \rho \boldsymbol u}{\partial t} + \nabla \cdot (\rho\boldsymbol u u + P) = \rho \boldsymbol f ~~,
$$

where $\gamma$ is the ratio of specific heats - typically $5/3$,
$\epsilon = \boldsymbol u^2 / 2 + e/\rho$, the total internal energy,
$e = P/(\gamma-1)$ is the internal energy density and $\boldsymbol f$ is
the Cartesian force vector. In each of the Euler equations the first
term refers to a conserved quantity, while the second term refers to a
flux vector $\textbf F$ - leading to the conservative form

<!--  Clear this up!!! -->
<!--  is it sensible to use \vec? perhaps change when converting to pdf  -->
$$
\frac{\partial q}{\partial t} + \nabla \cdot \textbf F = 0 ~~,
$$

where the flux vector is a function of the conserved quantities

$$
\textbf F =
\begin{pmatrix}
\rho \textbf u \\
p + \rho \textbf u u \\
\textbf u (E+p)
\end{pmatrix}
~,~
q =
\begin{pmatrix}
\rho \\
\rho \textbf u \\
E
\end{pmatrix} ~~,
$$

The Euler equations are coupled nonlinear partial differential
equations, hence no general solutions exist; as such, time dependent
numerical solutions must be used instead. Numerical solutions in the
simplest terms take the problem at its origin,

This is a flexible way of solving almost any problem involving coupled
differential equations, albeit at the cost of dramatically increased CPU
time and memory usage.

#### The Godunov Approach

The simplest method of solving the Euler equations in the hyperbolic
case is through the Godunov approach,

The Riemann problem represents a discontinuity in a fluid, with two sets
of conserved variables and an instantaneous change between then. In the
hyperbolic case, the initial conditions can be described thusly

<!--  word better -->
$$
\left.
\begin{array}{rl}
  \text{PDEs:} & \frac{\partial \textbf U}{\partial t} + \textbf{A} \frac{\partial \textbf U}{\partial x} = 0 ~,~ - \infty < x< \infty ~,~ t > 0 ~, \\
  \text{Initial Conditions: } & \textbf{U}(x,0) = \textbf{U}^{(0)} = \left\{
  \begin{array}{l}
    \textbf U_{\text L} ~ x < 0 ~,~ \\
    \textbf U_{\text R} ~ x > 0
  \end{array}
  \right.
\end{array} \right\}
$$

where $\textbf U$ is a vector of conserved variables, such as velocity,
pressure and density [@toro_riemann_2013]. This closely mimics the
Rankine-Huginot jump conditions describing a shock. This can be seen in
figure \ref{riemanncurve}

![The initial conditions of a riemann problem, where $\textbf U$ is a
conserved variable \label{riemanncurve}](assets/Riemann.png)

<!--  check this for accuracy, perhaps simplify? ask julian regarding this one -->
When broken into cells in a piecewise-constant fashion, a flow is
analogous to a series of Riemann problems; Godunov’s scheme does exactly
this, approximating the solution to the Euler equations via this series.
As such, solving the Euler equations in multiple dimensions involves
solving the Riemann problems for each face of a grid cell
[@wareing_hydrodynamical_2005]. Piecewise constant is the simplest form
that Godunov’s scheme can take, however higher-order extensions are
typically used instead, by interpolating cells using a piecewise linear
or piecewise parabolic method.

<!-- Read more of Wareings paper, and the Godunov paper cited by Julians thesis  -->
<!--  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem -->
#### Adaptive Mesh Refinement

A particular problem with numerical simulations was the sheer difficulty
of solving 3-dimensional problems; this is due to the exponential growth
of cells required as dimensionality is increased. While colliding winds
can be assumed to be cylindrically symmetric in the most basic cases,
introducing orbital motion will destroy axisymmetry due to the Coriolis
force, mandating 3-dimensional hydrodynamics for any simulation that
evolves as a function of time. In the specific case of CWB systems, the
problem is compounded, due to the large variance in length scales, from
sub-AU in the WCR to the parsec scale as the shock propagates out of the
system. Adaptive Mesh Refinement (AMR) sidesteps this conundrum by
changing the effective grid resolution as the simulatengad ion runs,
with an n-dimensional cell “refining” into $2^n$ cells when needed; the
condition for this refinement is if the Riemann solutions for the finest
cell and its “parent” cell are in disagreement.

![An example of AMR, more “complex” regions of the simulation are
refined, saving significant amounts of computational time and
memory](assets/amr.png)

<!-- Use a more up to date image of the grid array, perhaps make one in python? -->
In the structure of *mg*, a “join” array is used to connect cells and
levels together, meaning that there is a degree of CPU and memory
overhead; this is on top of the overhead of comparing fine cells with
their parent cells. However,5 this overhead is negligible compared to
computing all cells without AMR.

<!-- this may be a little clumsy an explanation? -->
<!-- Introduce a benchmark of some kind? For AMR vs non? -->
### Radiative Transfer

Literature Review
=================

While the previous section covers the background subject matter, this
section will cover two papers of particular importance in depth; the
first, *Pinwheels in the Sky, With Dust* [@hendrix_pinwheels_2016]
follows a similar premise to this project albeit with some differences.
*Radiation Driven Winds in Of Stars* [@castor_radiation-driven_1975]
describes the CAK theory, the seminal paper for [radiative line
driving](#driving-forces), which should be analysed in more detail in
order to better understand the underlying physics behind wind
propagation.

<!--  convert the hyperlink here to a /ref /label style  -->
<!-- A third paper should be used, if time allows, however, maybe use a different paper with a similar bent to mine, if one can be found  -->
Pinwheels in the Sky, With Dust
-------------------------------

*Pinwheels in the Sky* is a paper with a very similar premise to this
project, where a numerical simulation is used with the explicit purpose
of modelling the dust dynamics of the system WR 98a, to determine which
properties lead to its distinctive spiral structure. The main conclusion
of this paper was that cooling due to emission from the wind plasma was
not a key factor in the morphology of the wind collision region, hence
the system obeys adiabatic conditions. Furthermore the presence of
strong cooling causes thermal instabilities to dominate along the
region.

### Research & Conclusions

The outcome of this paper is significantly different to this project,
instead focusing on the formation of the spiral structure of WR 98a and
using dust for synthetic imaging to compare to archival observational
data, and to determine if the features would be detectable with future
telescopes. WR 98a is described as a prime target for interferometric
surveys, due to a variety of factors such as its large, spiral dust lane
which emits strongly in the infrared, in addition with in inclination of
$35^\circ$ to the line of sight, it can be well observed from ground
stations on Earth; WR 98a in addition has a short orbital period of
$1.4~\text{yr}$, meaning that multiple orbital passes can be observed
within a short time period, hence its dust forming periods can be
determined. This is not the case for some periodic dust forming CWB
systems, and is crucial for simulating periodic dust formation in the
simulations performed in this paper. Tables \ref{wr98atable} and
\ref{wr98atable2} contains system parameters and parameters for each
star.

<!--  Provide examples of periodic CWB systems and their orbital periods, perhaps do this in an earlier section, maybe the wolf rayet section? -->
  Parameter                                            WR                                                                                                                                                                                                                                    OB
  ---------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  $M$                                                  $10 ~\text M_\odot$                                                                                                                                                                                                                   $18 ~\text M_\odot$
  $\dot M$                                             $0.5\times 10^{-5} ~\text M_\odot \text{ yr}^{-1}$                                                                                                                                                                                    $0.5\times 10^{-7} ~\text M_\odot \text{ yr}^{-1}$
  $v_\infty$                                           $900 ~\text{km}\text{ s}^{-1}$                                                                                                                                                                                                        $2000 ~\text{km}\text{ s}^{-1}$

  : Properties of WR and OB star in WR 98a \label{wr98atable}

  Parameter           Value
  ------------------- --------------------------------------------------------
  $a$                 $6.08\times 10^{13} ~\text{cm} \approx 4.06~\text{au}$
  $P_\text{orb}$      $4.89\times 10^7 ~\text{s} \approx 1.55~\text{yr}$
  $\rho_\text{ISM}$   $10^{-20}~\text{g cm}^{-3}$

  : Properties of WR 98a system \label{wr98atable2}

<!--  These captions should be fixed up in the latex document -->
Similar to this project, an AMR code is used to simulate the colliding
wind binary system, as it is the only feasible, efficient method of
simulating this type of system, due to the small scale of the wind
collision region and the large overall system size. The size of the
simulation is $(320 \times 320 \times 140)a$, or approximately
$(1200\times 1200 \times 568) ~\text{au}$; this is a fairly compact
system, but is capable of simulating the wind collision region and the
spiral structure. The AMR simulation has 11 levels, leading to an
effective resolution of $(81920 \times 81920 \times 24576)$, this is
significantly higher resolution than any simulation yet attempted in
this project, which is reasonable considering the projects overall
progress; however the number of levels is interesting to note, and would
uniquely benefit the simulation of colliding wind binary systems, due to
their extremely small finest feature sizes.

The way that dust is handled in this paper is significantly different
from this project, providing insight into how the code should progress
beyond the initial version; as such section /ref{dustcomphendrix}
deconstructs the method behind the [@hendrix_pinwheels_2016] dust code
prescription.

![Keck I high angular resolution multiwavelength image of the pinwheel
nebula WR 98a [@hendrix_pinwheels_2016]
\label{wr98ahendrixkeck}](assets/Hendrix/stw1289fig1.jpeg)

![Synthetic images representative of WR 98a without cooling, this is
similar to observational data recorded by the Keck telescope
[@hendrix_pinwheels_2016]
\label{hendrixnocooling}](assets/Hendrix/stw1289fig12.jpeg)

Monte Carlo dusty radiative transfer is used to generate a series of
synthetic images in the infrared, as well as to perform virtual
photometry. This is performed in order to compare the simulations with
observational data, as well as to simulate the performance of more
modern telescopes when observational data from them was available.
Figure \ref{hendrixnocooling} demonstrates these virtual images, and how
emission wavelength and angular resolution can be adjusted.

The resultant data from this process shows that there is a correlation
between the adiabatic results from figure \ref{hendrixnocooling}, and
the observational results from the Keck telescope (figure
\ref{wr98ahendrixkeck}); when the system is radiative, thermal
instabilities along the wind collision region disrupt its notable spiral
shape, and lead to a more diffuse emission. While the Keck telescope
data is not of particularly high resolution, at the time of writing
there was no observational data from more contemporary telescopes, such
as the E-ELT or ALMA; if higher resolution observational data becomes
available during the span of this project, this could provide a way to
expand on this project, by revisiting some of the aspects of this paper
with better observational data and a more comprehensive dust model.

![Synthetic images representative of WR 98a with strong cooling, there
is a significant disruption of the wind collision region due to thermal
instabilities, as such it does not match observational data
[@hendrix_pinwheels_2016]
\label{hendrixcooling}](assets/Hendrix/stw1289fig14.jpeg)

### Comparison of Dust Mechanisms \label{dustcomphendrix}

Since this paper is one of the few papers that models dust within a
colliding wind binary system, a comparison of how dust is modelled
between this paper and the project is in order. Of important are the
underlying parameters, as well as the mechanism behind creation,
propagation and destruction of dust; in addition to this, its effect on
the environment, such as through cooling are also considered. The paper
covers most of these, using significantly different methods compared to
this project, while not including some of the routines for computational
simplicity and a lack of necessity.

#### Creation and Properties

Dust is created within the shock at a rate determined by observational
work on the star system attempting to be simulated, it is tied to an
empirical law, and is not based on the physical properties of the
system; furthermore dust is only created below a threshold orbital
separation to emulate the periodic nature of dust formation in most
observed systems. This can be justified by [@williams_infrared_1987],
which suggests that in multiple classes of WC systems exhibiting
periodicity only a minor change in density affects whether dust is
formed or not, causing the systems to behave as if they are in a
bistable equilibrium.

Dust grain sizes is based on an average of a distribution of dust grains
from [@yudin_speckle_2001], where the average grain radius
$\bar a_d = 15.44\times 10^{-7} \text{cm}$. While a grain size
distribution could be implemented, this would be significantly more
computationally complex, and would be unnecessary when factoring in the
goals of the project.

Currently, the multiple scalar method of this project can allow for an
arbitrary number of grain sizes, all modelled as separate scalars; these
scalars can be modified to simulate varying grain sizes, dust mass
fraction $z$ and bulk densities. This work is expanded upon in section
\ref{multiplescalarext}.

#### Propagation and simulation

Hendrix et al. utilise a multi-fluid method of modelling dust in their
simulation, having it act as a separate fluid to the stellar wind that
is coupled to the wind with the Epstein drag force

$$
f_d = (1-\alpha(T)) \sqrt \frac{8\gamma p}{\pi \rho} \frac{\rho \rho_d}{\rho_p a_d} (\boldsymbol v_d - \boldsymbol v) ~,
$$

where $\rho_d$ is the dust fluid density, $\rho_p$ is the grain bulk
density, $a_d$ is the grain size and $\alpha$ is the “sticking
coefficient” denoting the momentum transfer between dust and gas,
defined as

$$
\alpha = 0.35\exp\left( - \sqrt \frac{T T_0}{500~\text K} \right) +0.1 ~,
$$

This allows for the accurate modelling of the dynamics of larger grains,
which would require significantly more momentum to accelerate; however
this is a computationally expensive method due to the modelling of an
entirely separate fluid, and calculating a drag force that may be
unnecessary considering the conditions of the environment, where the
harsh environment would prevent large, heavy dust grains from forming.

<!--  sentence is a little overly long -->
At the moment, this project utilises an advected scalar method, where
the dust properties are a conserved quantity of the fluid and propagate
alongside it; this is physically accurate for small dust grains, in the
order of $10^{-3}\text - 10^{-2}~\mu \text m$ while significantly
reduces computational complexity and memory requirements, this can also
easily allow the modelling of multiple dust grains by simply iterating
the calculations over the number of scalars.

<!-- might make sense to clear this up a little bit, or talk about advected scalars earlier in the project than the next chapter  -->
<!-- is there a better way of doing dashes in latex math? -->
#### Routines not included

<!--  Change wording of this title -->
As the focus of this paper was on the circumstances leading to the
spiral structure of the WR98a wind collision region, many dust
behaviours were not included or considered, as they would have a
negligible impact on the wind structure.

<!--  circumstances? it's? -->
Dust cooling is not included in this project, and is instead used for
synthetic observation, in particular synthetic infrared imaging;
instead, plasma cooling is the only considered cooling phenomena. While
including dust cooling would improve the accuracy of the simulation, the
results of the paper indicated that the system was strongly adiabatic,
meaning that dust cooling did not need to be considered.

As with cooling, there are no routines for dust destruction, it is
assumed that during periods where dust is being created, the amount that
is created far exceeds the amount being destroyed; given the violent
conditions of a colliding wind binary system and its conduciveness to
dust destruction. However, since the dust production rate is calculated
from observational data, this would of course include the final amount
of dust being produced, which may result in a different distribution of
dust throughout the system, but would still be fairly accurate.

### Overall Comparison With Project

Project Outline
===============

Towards a Comprehensive Dust Model
----------------------------------

Simulation of Colliding Wind Binary Systems
-------------------------------------------

Comparison with Observational Data
----------------------------------

Progress
========

Dust Modelling
--------------

### Multiple Scalar Model

#### Extensions to the multiple scalar model \label{multiplescalarext}

Progression Beyond The First Year
=================================

Appendix
========

References {#references .unnumbered}
==========
