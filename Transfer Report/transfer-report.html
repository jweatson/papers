<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <meta name="author" content="Joseph Eatson" />
  <title>Transfer Report</title>
  <style type="text/css">code{white-space: pre;}</style>
  <link rel="stylesheet" href="web/template.css" type="text/css" />
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>
</head>
<body>
    <div class="navbar navbar-static-top">
    <div class="navbar-inner">
      <div class="container">
        <span class="doc-title">Transfer Report</span>
        <ul class="nav pull-right doc-info">
                    <li><p class="navbar-text">Joseph Eatson</p></li>
                              <li><p class="navbar-text">May-August 2018</p></li>
                  </ul>
      </div>
    </div>
  </div>
    <div class="container">
    <div class="row">
            <div id="TOC" class="span3">
        <div class="well toc">
        <ul>
          <li class="nav-header">Table of Contents</li>
        </ul>
        <ul>
        <li><a href="#summary">Summary</a></li>
        <li><a href="#background">Background</a><ul>
        <li><a href="#stellar-winds">Stellar Winds</a><ul>
        <li><a href="#driving-forces">Driving Forces</a></li>
        <li><a href="#seeking-an-effective-model-of-radiative-line-driving">Seeking an Effective Model of Radiative Line Driving</a></li>
        </ul></li>
        <li><a href="#computational-astrophysics">Computational Astrophysics</a><ul>
        <li><a href="#numerical-simulation">Numerical Simulation</a></li>
        </ul></li>
        </ul></li>
        <li><a href="#literature-review">Literature Review</a></li>
        <li><a href="#progress">Progress</a><ul>
        <li><a href="#dust-modelling">Dust Modelling</a><ul>
        <li><a href="#dust-creation">Dust Creation</a></li>
        <li><a href="#dust-propagation">Dust Propagation</a></li>
        <li><a href="#dust-destruction">Dust Destruction</a></li>
        <li><a href="#dust-cooling">Dust Cooling</a></li>
        </ul></li>
        <li><a href="#most-recent-model">Most Recent Model</a></li>
        <li><a href="#future-work">Future Work</a></li>
        </ul></li>
        </ul>
        </div>
      </div>
            <div class="span9">
            <h1 id="summary">Summary</h1>
<p>It is generally agreed upon that the bulk of dust formation in the galaxy occurs inside star systems containing Asymptotic Giant Branch (AGB) stars. However, there are additional formation processes that produce a non-insignificant amount of interstellar dust. One such process is dust production in Wind Collision Regions (WCR’s).</p>
<p>Wind Collision Regions form when the winds of two massive stars in a binary pair collide. The resultant shock heats the surrounding environment to temperatures exceeding <span class="math inline">\(10^8 \text K\)</span>. An area of particular interest is where the winds collide head on, producing the most energetic conditions in the system, however this area is extremely compact, occurring over an area smaller than <span class="math inline">\(1 \text{AU}\)</span>, this compactness, and the relative distance of nearest CWB systems excludes direct observation, meaning that numerical hydrodynamic simulation must be used.</p>
<h1 id="background">Background</h1>
<h2 id="stellar-winds">Stellar Winds</h2>
<p>Since the turn of the century, stellar winds have been observed by astronomers, and since the discovery of emission lines, spectroscopic data has been obtained for them. Doppler shifts of over 100 kilometres per second were viewed with scientific curiosity, and early photographic observations noted the slowly increasing angular diameter of the clouds - the idea of the universe as static and unchanging was being rapidly considered.</p>
<p>By the 1940’s, early mathematical models of these systems were being considered, however Underhill noted that radiation pressure alone was not sufficient to account for the sheer amount of material being ejected from OB stars - something was afoot, just out of sight.</p>
<p>In the second half of the century telescopes capable of seeing beyond visible light were built, opening up new avenues of observation; these new observatories showed us the infrared excesses of dust emission, and the strong X-ray emission of interstellar shocks - this comprehensive view of interstellar space allowed for the answering of long-standing questions.</p>
<h3 id="driving-forces">Driving Forces</h3>
<p>The primary radiative driving mechanism of low-mass stars is Thomson scattering, a form of elastic scattering where the photon interacts with a charged particle, changing the trajectory and velocity of the particle. This interaction is entirely dependent on the Thomson cross section, <span class="math inline">\(\sigma_t\)</span>, which helps determine the likelihood of a collision between a photon and a charged particle; in the case of an electron</p>
<p><span class="math display">\[
\sigma_t = \frac{8\pi}{3} \left( \frac{\alpha \hbar}{mc}\right)^2 \approx 6.652458 \times 10^{-29} \text m^2
\]</span> <!-- perhaps include another equation to show the interaction probability of Thomson scattering --></p>
<p>where <span class="math inline">\(\alpha\)</span> is the fine structure constant, <span class="math inline">\(1/137\)</span>. The cross section of an electron is extremely small, and even in the comparatively dense regime of a stellar wind, the opacity is not sufficient to drive fast, dense winds. For AGB stars, their low surface gravity results in the driving of extremely dense but slow winds. But for massive stars, capable of driving away the same amount of material but at speeds exceeding <span class="math inline">\(1000 \text{ km/s}\)</span>, there has to be a mechanism with a significantly greater opacity in order to drive the wind at the observed velocities and loss rates, while counteracting the significantly stronger surface gravity.</p>
<table>
<thead>
<tr class="header">
<th>Star</th>
<th><span class="math inline">\(v_\infty~(\text{km s}^{-1})\)</span></th>
<th><span class="math inline">\(\dot M~(\text{M}_\odot~\text{yr}^{-1})\)</span></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Solar mass star</td>
<td><span class="math inline">\(400-700\)</span></td>
<td><span class="math inline">\(10^{-14}\)</span></td>
</tr>
<tr class="even">
<td>AGB star</td>
<td><span class="math inline">\(10-30\)</span></td>
<td><span class="math inline">\(10^{-5}\)</span></td>
</tr>
<tr class="odd">
<td>OB star</td>
<td>1000-3000</td>
<td><span class="math inline">\(10^{-6} - 10^{-5}\)</span></td>
</tr>
</tbody>
</table>
<!-- Make this table more elaborate, add additional details, and wolf rayet stars to it -->
<p>Instead, massive stars utilise radiative line driving as the motive force behind their impressive winds. Hot stars emit large amounts of UV radiation at a variety of frequencies, some of which correspond with resonance lines of the elements in the wind; these resonance lines have an opacity <span class="math inline">\(10^6\)</span> times greater than Thomson scattering <span class="citation">(Lamers and Cassinelli <a href="#ref-lamers1999introduction">1999</a>)</span>; the rest of the wind is then accelerated with the ions via <em>Coulomb coupling</em>. Due to the limited number of resonant lines the motive force would be weaker than Thomson scattering if not for Doppler shift; as the wind velocity increases the photons are Doppler shifted in the reference frame of the atoms in the wind, resulting in a greater number of absorption events, and a greater motive force. The result of this phenomenon is increasing acceleration as the wind speeds increase (up to its terminal velocity), resulting in increased acceleration as the wind travels further from the star - the inverse of the Thomson scattering acceleration curve.</p>
<!-- Perhaps find a graph supporting this? -->
<p>The <em>mg</em> hydrodynamics code used in this project supports radiative driving, in addition to its default of using instantaneous acceleration. As the acceleration of the wind is gradual, reaching a terminal velocity at some distance from the star. For a binary system with a small separation distance this can result in a significant deviation in the structure of the collision; this is compounded by the effect of sudden radiative braking and stagnation-point flow, where radiative line driving from the secondary star acts on the flow from the primary star, rapidly braking it - these effects are further detailed in <span class="citation">(Stevens and Pollock <a href="#ref-stevens1994stagnation">1994</a>)</span> and <span class="citation">(Gayley, Owocki, and Cranmer <a href="#ref-gayley1997sudden">1997</a>)</span>.</p>
<!-- verify this -->
<h3 id="seeking-an-effective-model-of-radiative-line-driving">Seeking an Effective Model of Radiative Line Driving</h3>
<p>As previously discussed, line driving is the mechanism of choice for acceleration of dense, fast winds around massive stars; however, a highly accurate simulation capable of modelling every single line, including its scattering potential, and the path it takes through the wind , would fall under the purview of ray-tracing for every single time step of a simulation, something that is not computationally feasible.</p>
<p>Parameterising the</p>
<p>This is also a good primer for explaining a core tenant of effective numerical simulation, the trade-off between physical accuracy and simulation complexity.</p>
<h4 id="the-sobolev-approximation">The Sobolev Approximation</h4>
<h4 id="the-cak-formalism">The CAK Formalism</h4>
<h4 id="the-finite-disk-correction-factor">The Finite Disk Correction Factor</h4>
<h2 id="computational-astrophysics">Computational Astrophysics</h2>
<h3 id="numerical-simulation">Numerical Simulation</h3>
<p>Inviscid, compressible flows, such as the movement of atoms and molecules through the interstellar medium are governed by the Euler equations, which in vector form are</p>
<p><span class="math display">\[
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0
\]</span></p>
<p><span class="math display">\[
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0
\]</span></p>
<p><span class="math display">\[
\frac{\partial \rho \boldsymbol u}{\partial t} + \nabla \cdot (\rho\boldsymbol u u + P) = \rho \boldsymbol f
\]</span></p>
<!-- should the following 3 equations be included in this group? -->
<p><span class="math display">\[
\gamma = 5/3
\]</span></p>
<p><span class="math display">\[
e = P/(\gamma-1)
\]</span></p>
<p><span class="math display">\[
\epsilon = \boldsymbol u^2 / 2 + e/\rho
\]</span></p>
<p>where <span class="math inline">\(\gamma\)</span> is the ratio of specific heats, <span class="math inline">\(\epsilon\)</span> is the total internal energy, <span class="math inline">\(e\)</span> is the internal energy density and <span class="math inline">\(\boldsymbol f\)</span> is the Cartesian force vector. In each of the Euler equations the first term refers to a conserved quantity, while the second term refers to a flux vector <span class="math inline">\(\textbf F\)</span>; leading to the conservative form</p>
<!--  is it sensible to use \vec? perhaps change when converting to pdf  -->
<p><span class="math display">\[
\frac{\partial q}{\partial t} + \nabla \cdot \textbf F = 0
\]</span></p>
<p>Where the flux vector is a function of the conserved quantities</p>
<p><span class="math display">\[
\textbf F =
\begin{pmatrix}
\rho \textbf u \\
p + \rho \textbf u u \\
\textbf u (E+p)
\end{pmatrix}
~,~
q =
\begin{pmatrix}
\rho \\
\rho \textbf u \\
E
\end{pmatrix}
\]</span></p>
<p>The Euler equations are coupled nonlinear partial differential equations, hence no general solutions exist; as such, time dependent numerical solutions must be used instead. Numerical solutions in the simplest terms take the problem at its origin,</p>
<p>This is a flexible way of solving almost any problem involving coupled differential equations, albeit at the cost of dramatically increased CPU time and memory usage.</p>
<h4 id="the-godunov-approach">The Godunov Approach</h4>
<p>The simplest method of solving the Euler equations in the hyperbolic case is through the Godunov approach,</p>
<p>The Riemann problem represents a discontinuity in a fluid, with two sets of conserved variables and an instantaneous change between then. In the hyperbolic case, the initial conditions can be described thusly</p>
<!--  word better -->
<p><span class="math display">\[
\left.
\begin{array}{rl}
  \text{PDEs:} &amp; \frac{\partial \textbf U}{\partial t} + \textbf{A} \frac{\partial \textbf U}{\partial x} = 0 ~,~ - \infty &lt; x&lt; \infty ~,~ t &gt; 0 ~, \\
  \text{Initial Conditions: } &amp; \textbf{U}(x,0) = \textbf{U}^{(0)} = \left\{
  \begin{array}{l}
    \textbf U_{\text L} ~ x &lt; 0 ~,~ \\
    \textbf U_{\text R} ~ x &gt; 0
  \end{array}
  \right.
\end{array} \right\}
\]</span></p>
<p>where <span class="math inline">\(\textbf U\)</span> is a vector of conserved variables, such as velocity, pressure and density <span class="citation">(Toro <a href="#ref-toro2013riemann">2013</a>)</span>. This closely mimics the Rankine-Huginot jump conditions describing a shock.</p>
<div class="figure">
<img src="assets/Riemann.png" alt="The initial conditions of a riemann problem, where \textbf U is a conserved variable" />
<p class="caption">The initial conditions of a riemann problem, where <span class="math inline">\(\textbf U\)</span> is a conserved variable</p>
</div>
<!--  check this for accuracy, perhaps simplify? ask julian regarding this one -->
<p>When broken into cells in a piecewise-constant fashion, a flow is analogous to a series of Riemann problems; Godunov’s scheme does exactly this, approximating the solution to the Euler equations via this series. As such, solving the Euler equations in multiple dimensions involves solving the Riemann problems for each face of a grid cell <span class="citation">(Wareing <a href="#ref-wareing2005hydrodynamical">2005</a>)</span>. Piecewise constant is the simplest form that Godunov’s scheme can take, however higher-order extensions are typically used instead, by interpolating cells using a piecewise linear or piecewise parabolic method.</p>
<!-- Read more of Wareings paper, and the Godunov paper cited by Julians thesis  -->
<!--  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem -->
<h4 id="adaptive-mesh-refinement">Adaptive Mesh Refinement</h4>
<p>A particular problem with numerical simulations was the sheer difficulty of solving 3-dimensional problems; this is due to the exponential growth of cells required as dimensionality is increased. While colliding winds can be assumed to be cylindrically symmetric in the most basic cases, introducing orbital motion will destroy axisymmetry due to the Coriolis force, mandating 3-dimensional hydrodynamics for any simulation that evolves as a function of time. In the specific case of CWB systems, the problem is compounded, due to the large variance in length scales, from sub-AU in the WCR to the parsec scale as the shock propagates out of the system. Adaptive Mesh Refinement (AMR) sidesteps this conundrum by changing the effective grid resolution as the simulation runs, with an n-dimensional cell “refining” into <span class="math inline">\(2^n\)</span> cells when needed; the condition for this refinement is if the Riemann solutions for the finest cell and its “parent” cell are in disagreement.</p>
<div class="figure">
<img src="assets/amr.png" alt="An example of AMR, more complex regions of the simulation are refined, saving significant amounts of computational time and memory" />
<p class="caption">An example of AMR, more “complex” regions of the simulation are refined, saving significant amounts of computational time and memory</p>
</div>
<!-- Use a more up to date image of the grid array, perhaps make one in python? -->
<p>In the structure of <em>mg</em>, a “join” array is used to connect cells and levels together, meaning that there is a degree of CPU and memory overhead; this is on top of the overhead of comparing fine cells with their parent cells. However,5 this overhead is negligible compared to computing all cells without AMR.</p>
<!-- this may be a little clumsy an explanation? -->
<!-- Introduce a benchmark of some kind? For AMR vs non? -->
<h1 id="literature-review">Literature Review</h1>
<h1 id="progress">Progress</h1>
<h2 id="dust-modelling">Dust Modelling</h2>
<p>Multiple models are to be included in this project for a comprehensive model of dust creation and destruction in colliding wind binary systems - such models include:</p>
<ul>
<li>Grain-grain collision</li>
<li>UV sputtering</li>
</ul>
<h3 id="dust-creation">Dust Creation</h3>
<h3 id="dust-propagation">Dust Propagation</h3>
<p>The current model for dust propagation is at this point very simplistic, the density ratio of dust to gas <span class="math inline">\(n_{\text{dust}}\)</span> is stored as an advected scalar which propagates with the wind; this obeys the assumption that the dust co-moves with the wind. Because of this model, it is not realistic to model grain-grain collisions, hence gas-grain sputtering is considered first, as a more complex model will be required for grain-grain collisions.</p>
<p>Furthermore</p>
<h3 id="dust-destruction">Dust Destruction</h3>
<p>As previously stated, the only destruction model currently used is the gas-grain collision model, which is based on the Draine-Salpeter prescription <span class="citation">(Draine and Salpeter <a href="#ref-draine1979destruction">1979</a>)</span></p>
<p>It is assumed that a grain has a mean lifespan of</p>
<p><span class="math display">\[
\tau_\text{grain} \equiv 1~\text{Myr} \cdot \frac{a~(\mu \text m)}{n}
\]</span></p>
<p>Where a grain has a mass of</p>
<p><span class="math display">\[
m = \frac{4}{3} \pi a^3 \rho_\text{grain}
\]</span></p>
<p>Hence, mass-loss rate per grain can be found to be</p>
<p><span class="math display">\[
\dot m = \frac{m}{\tau_\text{grain}}
= \frac{4}{3} \frac{\pi a^3 n \rho_\text{grain} }{3.156\times10^{17}}
= 1.33 1.33\times10^{-17}
\]</span></p>
<p>This is not a general solution, this assumes a grain of size <span class="math inline">\(0.1~\mu\text m\)</span></p>
<h3 id="dust-cooling">Dust Cooling</h3>
<p>One of the goals of this project is to determine the effect of dust on the outgoing wind, hence, the inclusion of radiative cooling in the simulation is vital. In this simulation cooling is calculated through the application of a cooling curve, to calculate the energy loss of the fluid through the equation</p>
<p><span class="math display">\[
\Delta E = - \frac{\rho^2}{m_H^2}\Lambda_c
\]</span></p>
<p>Where <span class="math inline">\(\Delta E\)</span> is the change in energy per time step per cell and <span class="math inline">\(\Lambda\)</span> is the local cooling coefficient. In mg the coefficient is found from a lookup table generated by cooling curves, an example can be seen in figure </p>
<div class="figure">
<img src="assets/coolcurvenormalised.png" alt="Normalised cooling curve for dust emission compared to a solar cooling curve " />
<p class="caption">Normalised cooling curve for dust emission compared to a solar cooling curve </p>
</div>
<p>Including dust based cooling could be done in one of two fashions, by altering the current function, <code>radiate.c</code> to read a second lookup table based on a dust emission cooling curve and calculate a unified cooling parameter, or calculating dust cooling with an entirely different function - the latter would be preferable to</p>
<h2 id="most-recent-model">Most Recent Model</h2>
<!-- this will not be included in the final transfer report -->
<p>Testing was initially performed with a low-resolution model in order to facilitate faster testing,</p>
<p>After this</p>
<p>Currently only a symmetric case is being considered, in order to reduce the number of cells required for a large-scale model.</p>
<p>The high resolution grid model uses a resolution of <span class="math inline">\((1500\times250)\)</span>, with a level depth of <span class="math inline">\(6\)</span></p>
<p>The model parameters are as follows:</p>
<table>
<thead>
<tr class="header">
<th>Parameter</th>
<th>Star a&amp;b Value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><span class="math inline">\(M_\star\)</span></td>
<td><span class="math inline">\(19.2 ~M_\odot\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(\dot M\)</span></td>
<td><span class="math inline">\(2.52 \times 10^{-5} ~M_\odot ~\text{yr}^{-1}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(v_\infty\)</span></td>
<td><span class="math inline">\(1.4 \times 10^8 ~\text{cm}~\text{s}^{-1}\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(T_\text{wind}\)</span></td>
<td><span class="math inline">\(10^4~\text{K}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(d_\text{sep}\)</span></td>
<td><span class="math inline">\(10^4~\text{K}\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(\eta\)</span></td>
<td><span class="math inline">\(1\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(\chi\)</span></td>
<td><span class="math inline">\(60.0104\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(\rho_\text{ISM}\)</span></td>
<td><span class="math inline">\(10^{-21} ~\text{g}~\text{cm}^{-3}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(T_\text{ISM}\)</span></td>
<td><span class="math inline">\(10^4 ~\text{K}\)</span></td>
</tr>
</tbody>
</table>
<p>Instantaneous velocity is assumed, and due to the high value of <span class="math inline">\(\chi\)</span> the system is not assumed to be adiabatic, hence, cooling is enabled, utilising the solar cooling curve.</p>
<h2 id="future-work">Future Work</h2>
<p>After suitable parameters for grain-gas sputtering are used, adding an additional dust destruction mechanism should be the first priority; in particular, UV sputtering, as the intense UV flux of both stars should lead to more impressive results. A parameter based approach, similar to the grain-gas sputtering currently used would be ideal, in order to preserve</p>
<div class="figure">
<img src="assets/output_3_0.png" alt="0.1 \mu \text m" />
<p class="caption"><span class="math inline">\(0.1 \mu \text m\)</span></p>
</div>
<div id="refs" class="references">
<div id="ref-draine1979destruction">
<p>Draine, BT, and EE Salpeter. 1979. “Destruction Mechanisms for Interstellar Dust.” <em>The Astrophysical Journal</em> 231: 438–55.</p>
</div>
<div id="ref-gayley1997sudden">
<p>Gayley, KG, SP Owocki, and SR Cranmer. 1997. “Sudden Radiative Braking in Colliding Hot-Star Winds.” <em>The Astrophysical Journal</em> 475 (2). IOP Publishing: 786.</p>
</div>
<div id="ref-lamers1999introduction">
<p>Lamers, Henny JGLM, and Joseph P Cassinelli. 1999. <em>Introduction to Stellar Winds</em>. Cambridge university press.</p>
</div>
<div id="ref-stevens1994stagnation">
<p>Stevens, Ian R, and AMT Pollock. 1994. “Stagnation-Point Flow in Colliding-Wind Binary Systems.” <em>Monthly Notices of the Royal Astronomical Society</em> 269 (2). Oxford University Press Oxford, UK: 226–34.</p>
</div>
<div id="ref-toro2013riemann">
<p>Toro, Eleuterio F. 2013. <em>Riemann Solvers and Numerical Methods for Fluid Dynamics: A Practical Introduction</em>. Springer Science &amp; Business Media.</p>
</div>
<div id="ref-wareing2005hydrodynamical">
<p>Wareing, Christopher J. 2005. <em>Hydrodynamical Modelling of Planetary Nebulae and Their Interaction with the Interstellar Medium</em>.</p>
</div>
</div>
            </div>
    </div>
  </div>
</body>
</html>
