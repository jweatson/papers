=== TABLE OF CONTENTS ===
00-Summary.md:1:# Summary
01-Background.md:1:# Background
01-Background.md:3:## Stellar Winds
01-Background.md:10:### Driving Forces
01-Background.md:37:### Seeking an Effective Model of Radiative Line Driving
01-Background.md:45:#### The CAK Formalism
01-Background.md:52:#### The Sobolev Approximation
01-Background.md:55:#### The Finite Disk Correction Factor
01-Background.md:70:## Massive Binary Star Systems
01-Background.md:72:### Wolf-Rayet Stars
01-Background.md:79:## Computational Astrophysics
01-Background.md:82:### Numerical Simulation
01-Background.md:131:#### The Godunov Approach
01-Background.md:165:#### Adaptive Mesh Refinement
01-Background.md:180:### Radiative Transfer
02-LiteratureReview.md:1:# Literature Review
02-LiteratureReview.md:8:## Pinwheels in the Sky, With Dust
02-LiteratureReview.md:11:### Research & Conclusions
02-LiteratureReview.md:49:### Comparison of Dust Mechanisms \label{dustcomphendrix}
02-LiteratureReview.md:52:#### Creation and Properties
02-LiteratureReview.md:59:#### Propagation and simulation
02-LiteratureReview.md:82:#### Routines not included
02-LiteratureReview.md:92:### Overall Comparison With Project
03-Outline.md:1:# Project Outline
03-Outline.md:3:## Towards a Comprehensive Dust Model
03-Outline.md:5:## Simulation of Colliding Wind Binary Systems
03-Outline.md:7:## Comparison with Observational Data
04-Progress.md:1:# Progress
04-Progress.md:3:## Dust Modelling
04-Progress.md:5:### Multiple Scalar Model
04-Progress.md:7:#### Extensions to the multiple scalar model \label{multiplescalarext}
05-Futurework.md:1:# Progression Beyond The First Year
99-EndMatter.md:1:# Appendix
99-EndMatter.md:3:# References
