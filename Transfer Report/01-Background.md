# Background

## Stellar Winds
Since the turn of the century, stellar winds have been observed by astronomers, and since the discovery of emission lines, spectroscopic data has been obtained for them. Doppler shifts of over 100 kilometres per second were viewed with scientific curiosity, and early photographic observations noted the slowly increasing angular diameter of the clouds - the idea of the universe as static and unchanging was being rapidly considered.

By the 1940's, early mathematical models of these systems were being considered, however Underhill noted that radiation pressure alone was not sufficient to account for the sheer amount of material being ejected from OB stars - something was afoot, just out of sight.

In the second half of the century telescopes capable of seeing beyond visible light were built, opening up new avenues of observation; these new observatories showed us the infrared excesses of dust emission, and the strong X-ray emission of interstellar shocks - this comprehensive view of interstellar space allowed for the answering of long-standing questions.

### Driving Forces
The primary radiative driving mechanism of low-mass stars is Thompson scattering, a form of elastic scattering where the photon interacts with a charged particle, changing the trajectory and velocity of the particle. This interaction is entirely dependent on the Thomson cross section, $\sigma_t$, which helps determine the likelihood of a collision between a photon and a charged particle; in the case of an electron

$$
\sigma_t = \frac{8\pi}{3} \left( \frac{\alpha \hbar}{mc}\right)^2 \approx 6.652458 \times 10^{-29} \text m^2 ~~,
$$
<!-- perhaps include another equation to show the interaction probability of Thomson scattering -->

where $\alpha$ is the fine structure constant, $1/137$. The cross section of an electron is extremely small, and even in the comparatively dense regime of a stellar wind, the opacity is not sufficient to drive fast, dense winds. For AGB stars, their low surface gravity results in the driving of extremely dense but slow winds. But for massive stars, capable of driving away the same amount of material but at speeds exceeding $1000 \text{ km/s}$, there has to be a mechanism with a significantly greater opacity in order to drive the wind at the observed velocities and loss rates, while counteracting the significantly stronger surface gravity.

Star            | $v_\infty~(\text{km s}^{-1})$ | $\dot M~(\text{M}_\odot~\text{yr}^{-1})$
----------------|---------------------------|--------------------------------------
Solar mass star | $400-700$                 | $10^{-14}$
AGB star        | $10-30$                   | $10^{-5}$
OB star         | 1000-3000                 | $10^{-6} - 10^{-5}$

<!-- Make this table more elaborate, add additional details, and wolf rayet stars to it -->

Instead, massive stars utilise radiative line driving as the motive force behind their impressive winds. Hot stars emit large amounts of UV radiation at a variety of frequencies, some of which correspond with resonance lines of the elements in the wind; these resonance lines have an opacity $10^6$ times greater than Thomson scattering [@lamers_introduction_1999]; the rest of the wind is then accelerated with the ions via _Coulomb coupling_. Due to the limited number of resonant lines the motive force would be weaker than Thomson scattering if not for Doppler shift; as the wind velocity increases the photons are Doppler shifted in the reference frame of the atoms in the wind, resulting in a greater number of absorption events, and a greater motive force. The result of this phenomenon is increasing acceleration as the wind speeds increase (up to its terminal velocity), resulting in increased acceleration as the wind travels further from the star - the inverse of the Thomson scattering acceleration curve.

<!-- Perhaps find a graph supporting this? -->


The _mg_ hydrodynamics code used in this project supports radiative driving, in addition to its default of using instantaneous acceleration. As the acceleration of the wind is gradual, reaching a terminal velocity at some distance from the star. For a binary system with a small separation distance this can result in a significant deviation in the structure of the collision; this is compounded by the effect of sudden radiative braking and stagnation-point flow, where radiative line driving from the secondary star acts on the flow from the primary star, rapidly braking it - these effects are further detailed in [@stevens_stagnation-point_1994] and [@gayley_sudden_1997].

<!-- verify this -->

### Seeking an Effective Model of Radiative Line Driving

As previously discussed, line driving is the mechanism of choice for acceleration of dense, fast winds around massive stars; however, a highly accurate simulation capable of modelling every single line, including its scattering potential, and the path it takes through the wind , would fall under the purview of ray-tracing for every single time step of a simulation, something that is not computationally feasible.

Parameterising the equations reduces computational complexity, and allows this to be calculated at every step in a hydrodynamical simulation effectively; however, careful consideration must be made regarding phenomena, in order for it to be effectively modelled. This is also a good example of a core tenant of effective numerical simulation, the trade-off between physical accuracy and simulation complexity.

<!--  This is very much a placeholder paragraph, especially regarding parameterisation -->

#### The CAK Formalism
The CAK formalism on the other hand seeks to parametrise the force exerted on stellar wind material as a whole,

With these models, there exist critical points where line driving is maximised due to

This formalism forms the basis behind all radiatively driven wind models for massive stars,

#### The Sobolev Approximation


#### The Finite Disk Correction Factor
A final modification to the CAK formalism is considering the effect of proximity to the star has on the photon flux received by the stellar material, if the star is assumed to be a uniformly radiating surface, then the photon flux decreases at close proximity.

At a distance however, assuming that each photon performs a single scattering event, there are more available photons, leading to an increase in flux - and an increase in force; combined with the decreased force due to gravity from the star, this leads to a significant increase in final velocity, explaining discrepancies from the earlier model.

The finite disk correction factor is defined as

$$
f = \frac{(1+\sigma)^{1+\alpha} - (1+\sigma \mu_c^2)^{1+\alpha}}
{(1+\alpha)(1-\mu_c^2)\sigma(1+\sigma)^\alpha}~,
$$

where $\sigma = d\ln(v/d)\ln(r-1)$, $\mu^2_c = 1-(R^2/r^2)$ and $\alpha$ is the CAK parameter characterising the distribution of optically thick optically thin lines [@friend_theory_1986].


## Massive Binary Star Systems

### Wolf-Rayet Stars
Wolf-Rayet stars, discovered by Charles Wolf and Georges Rayet in 1867, are characterised by their extremely broad emission lines; the classification can be further subdivided into WN, WC and WO subtypes through the presence of a strong secondary nitrogen, carbon or oxygen line.

The reason for these strong emission lines is due to the



## Computational Astrophysics
Due to the small scale of the system and the comparative distance of CWB systems, most analysis of these systems is done using simulations, which are then compared to the scarce available observational data. Typically the wind structure of the system is performed with [numerical simulation](#numerical-simulation) and the fluid <!-- is this a good word for it? --> properties of the system are used to derive observable properties using [radiative transfer](#radiative-transfer)

### Numerical Simulation
Inviscid, compressible flows, such as the movement of atoms and molecules through the interstellar medium are governed by the Euler equations, which in vector form are

$$
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
$$

$$
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
$$

$$
\frac{\partial \rho \boldsymbol u}{\partial t} + \nabla \cdot (\rho\boldsymbol u u + P) = \rho \boldsymbol f ~~,
$$

where $\gamma$ is the ratio of specific heats - typically $5/3$, $\epsilon = \boldsymbol u^2 / 2 + e/\rho$, the total internal energy, $e = P/(\gamma-1)$ is the internal energy density and $\boldsymbol f$ is the Cartesian force vector. In each of the Euler equations the first term refers to a conserved quantity, while the second term refers to a flux vector $\textbf F$ - leading to the conservative form

<!--  Clear this up!!! -->


<!--  is it sensible to use \vec? perhaps change when converting to pdf  -->

$$
\frac{\partial q}{\partial t} + \nabla \cdot \textbf F = 0 ~~,
$$

where the flux vector is a function of the conserved quantities

$$
\textbf F =
\begin{pmatrix}
\rho \textbf u \\
p + \rho \textbf u u \\
\textbf u (E+p)
\end{pmatrix}
~,~
q =
\begin{pmatrix}
\rho \\
\rho \textbf u \\
E
\end{pmatrix} ~~,
$$


The Euler equations are coupled nonlinear partial differential equations, hence no general solutions exist; as such, time dependent numerical solutions must be used instead. Numerical solutions in the simplest terms take the problem at its origin,

This is a flexible way of solving almost any problem involving coupled differential equations, albeit at the cost of dramatically increased CPU time and memory usage.

#### The Godunov Approach
The simplest method of solving the Euler equations in the hyperbolic case is through the Godunov approach,

The Riemann problem represents a discontinuity in a fluid, with two sets of conserved variables and an instantaneous change between then. In the hyperbolic case, the initial conditions can be described thusly

<!--  word better -->


$$
\left.
\begin{array}{rl}
  \text{PDEs:} & \frac{\partial \textbf U}{\partial t} + \textbf{A} \frac{\partial \textbf U}{\partial x} = 0 ~,~ - \infty < x< \infty ~,~ t > 0 ~, \\
  \text{Initial Conditions: } & \textbf{U}(x,0) = \textbf{U}^{(0)} = \left\{
  \begin{array}{l}
    \textbf U_{\text L} ~ x < 0 ~,~ \\
    \textbf U_{\text R} ~ x > 0
  \end{array}
  \right.
\end{array} \right\}
$$

where $\textbf U$ is a vector of conserved variables, such as velocity, pressure and density [@toro_riemann_2013]. This closely mimics the Rankine-Huginot jump conditions describing a shock. This can be seen in figure \ref{riemanncurve}

![The initial conditions of a riemann problem, where $\textbf U$ is a conserved variable \label{riemanncurve}](assets/Riemann.png)

<!--  check this for accuracy, perhaps simplify? ask julian regarding this one -->

When broken into cells in a piecewise-constant fashion, a flow is analogous to a series of Riemann problems; Godunov's scheme does exactly this, approximating the solution to the Euler equations via this series. As such, solving the Euler equations in multiple dimensions involves solving the Riemann problems for each face of a grid cell [@wareing_hydrodynamical_2005]. Piecewise constant is the simplest form that Godunov's scheme can take, however higher-order extensions are typically used instead, by interpolating cells using a piecewise linear or piecewise parabolic method.

<!-- Read more of Wareings paper, and the Godunov paper cited by Julians thesis  -->


<!--  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem -->

#### Adaptive Mesh Refinement

A particular problem with numerical simulations was the sheer difficulty of solving 3-dimensional problems; this is due to the exponential growth of cells required as dimensionality is increased. While colliding winds can be assumed to be cylindrically symmetric in the most basic cases, introducing orbital motion will destroy axisymmetry due to the Coriolis force, mandating 3-dimensional hydrodynamics for any simulation that evolves as a function of time. In the specific case of CWB systems, the problem is compounded, due to the large variance in length scales, from sub-AU in the WCR to the parsec scale as the shock propagates out of the system. Adaptive Mesh Refinement (AMR) sidesteps this conundrum by changing the effective grid resolution as the simulatengad
ion runs, with an n-dimensional cell "refining" into $2^n$ cells when needed; the condition for this refinement is if the Riemann solutions for the finest cell and its "parent" cell are in disagreement.

![An example of AMR, more "complex" regions of the simulation are refined, saving significant amounts of computational time and memory](assets/amr.png)

<!-- Use a more up to date image of the grid array, perhaps make one in python? -->

In the structure of _mg_, a "join" array is used to connect cells and levels together, meaning that there is a degree of CPU and memory overhead; this is on top of the overhead of comparing fine cells with their parent cells. However,5 this overhead is negligible compared to computing all cells without AMR.

<!-- this may be a little clumsy an explanation? -->

<!-- Introduce a benchmark of some kind? For AMR vs non? -->

### Radiative Transfer
