01-Background.md:16: perhaps include another equation to show the interaction probability of Thomson scattering 
01-Background.md:26: Make this table more elaborate, add additional details, and wolf rayet stars to it 
01-Background.md:30: Perhaps find a graph supporting this? 
01-Background.md:35: verify this 
01-Background.md:43:  This is very much a placeholder paragraph, especially regarding parameterisation 
01-Background.md:80:Due to the small scale of the system and the comparative distance of CWB systems, most analysis of these systems is done using simulations, which are then compared to the scarce available observational data. Typically the wind structure of the system is performed with [numerical simulation](#numerical-simulation) and the fluid  is this a good word for it?  properties of the system are used to derive observable properties using [radiative transfer](#radiative-transfer)
01-Background.md:99:  Clear this up!!! 
01-Background.md:102:  is it sensible to use \vec? perhaps change when converting to pdf  
01-Background.md:136:  word better 
01-Background.md:156:  check this for accuracy, perhaps simplify? ask julian regarding this one 
01-Background.md:160: Read more of Wareings paper, and the Godunov paper cited by Julians thesis  
01-Background.md:163:  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem 
01-Background.md:172: Use a more up to date image of the grid array, perhaps make one in python? 
01-Background.md:176: this may be a little clumsy an explanation? 
01-Background.md:178: Introduce a benchmark of some kind? For AMR vs non? 
02-LiteratureReview.md:4:  convert the hyperlink here to a /ref /label style  
02-LiteratureReview.md:6: A third paper should be used, if time allows, however, maybe use a different paper with a similar bent to mine, if one can be found  
02-LiteratureReview.md:14:  Provide examples of periodic CWB systems and their orbital periods, perhaps do this in an earlier section, maybe the wolf rayet section? 
02-LiteratureReview.md:32:  These captions should be fixed up in the latex document 
02-LiteratureReview.md:74:  sentence is a little overly long 
02-LiteratureReview.md:78: might make sense to clear this up a little bit, or talk about advected scalars earlier in the project than the next chapter  
02-LiteratureReview.md:80: is there a better way of doing dashes in latex math? 
02-LiteratureReview.md:83:  Change wording of this title 
02-LiteratureReview.md:86:  circumstances? it's? 
multidust-progress.md:79: this will not be included in the final transfer report 
transfer-report.md:111: perhaps include another equation to show the interaction probability of Thomson scattering 
transfer-report.md:130: Make this table more elaborate, add additional details, and wolf rayet stars to it 
transfer-report.md:147: Perhaps find a graph supporting this? 
transfer-report.md:159: verify this 
transfer-report.md:176:  This is very much a placeholder paragraph, especially regarding parameterisation 
transfer-report.md:235: is this a good word for it?  properties of the system are used
transfer-report.md:264:  Clear this up!!! 
transfer-report.md:265:  is it sensible to use \vec? perhaps change when converting to pdf  
transfer-report.md:306:  word better 
transfer-report.md:328:  check this for accuracy, perhaps simplify? ask julian regarding this one 
transfer-report.md:339: Read more of Wareings paper, and the Godunov paper cited by Julians thesis  
transfer-report.md:340:  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem 
transfer-report.md:362: Use a more up to date image of the grid array, perhaps make one in python? 
transfer-report.md:369: this may be a little clumsy an explanation? 
transfer-report.md:370: Introduce a benchmark of some kind? For AMR vs non? 
transfer-report.md:386:  convert the hyperlink here to a /ref /label style  
transfer-report.md:387: A third paper should be used, if time allows, however, maybe use a different paper with a similar bent to mine, if one can be found  
transfer-report.md:420:  Provide examples of periodic CWB systems and their orbital periods, perhaps do this in an earlier section, maybe the wolf rayet section? 
transfer-report.md:437:  These captions should be fixed up in the latex document 
transfer-report.md:559:  sentence is a little overly long 
transfer-report.md:568: might make sense to clear this up a little bit, or talk about advected scalars earlier in the project than the next chapter  
transfer-report.md:569: is there a better way of doing dashes in latex math? 
transfer-report.md:572:  Change wording of this title 
transfer-report.md:578:  circumstances? it's? 
