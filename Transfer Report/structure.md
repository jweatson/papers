# Structure and Notes for Transfer Report
# General Notes
Estimate of size: around 25 pages, 12500 words, this should be an adequate length for the project.

# Summary

- Dust formation in Wolf-Rayet binary pairs
- Produces over 1e-8 solar masses worth of dust
- Limited number of binary pairs, but may provide a slight contribution to the overall amount of dust in the galaxy
- While dust has been observed, there is a limited amount of literature surrounding the formation mechanisms
- Direct observation difficult, due to the compact nature of the WCR
- Hence numerical simulation, with additional ray-trace calculations to be used, and compared to observational findings

# Background
Background should be one of the three major components, I believe it will be the longest one overall, as the underlying science should be clearly detailed:

- This section will cover
  - Stellar winds
  - Stellar environment
  - Wolf-Rayet

## Stellar Winds

- Discuss the importance of stellar winds in the galactic environment
  - Touch on AGB stars as the primary formation site of AGB stars
  - Radiative driving, impulsive methods
    - Thomson scattering
    - Dust driving (not to be considered in this work, due to line driving being significantly more effective)
    - Line driving

## The Stellar Environment Being Considered

### Binary Systems
### Wolf-Rayet Stars
### Colliding Wind Binary Systems


## Important Physical Processes

### Dust Formation

### Shock Cooling


## Simulation
Discuss why numerical simulation is required, and what it entails

### Numerical Simulations
- Why use numerical simulation?
  - Partial differential equations are bastards
- A simplistic overview
  - The fluid equation
  - Simple models, piecewise constant method
  - Touch on SPH, and why it is not used
  - Piecewise linear and piecewise parabolic methods
- The Godunov scheme, conservative numerical scheme
- Addition of features, the balance between what would be considered accurate or not
  - Use an example, no self-gravity in code, but there is radiative transfer
- Adaptive mesh refinement
  - The `mg` code

### Other Simulation Aspects
- Radiative transfer, simplistic explanation
- Ray-tracing code

# Literature Review
There is not a particularly large amount of literature on the subject, and even less so on dust formation and destruction in these systems.

Avoid using review papers in this section, as that is not what they are for

What differentiates this project from the literature? Now is a good time to answer this question

# Progress Report
- Initial work with ARWEN
  - Why ARWEN was not used in the case of
- Early hindrances of work
  - Mainly understanding how the code works, early models were not running correctly for various reasons
    - Adaptive mesh refinement failing, or using order of magnitude more memory to allocate cells at the beginning, defeating the purpose of using AMR
- Current state of the model
  - Use of instantaneous velocity over radiative driving
  - Accurate scales
  - Advected scalar model of dust
    - How scalars can be modelled by tying them into the main fluid
    - Fluid coupling, is it accurate?
  - Inclusion of additional destruction methods
    - Grain-grain collision operates on a long time scale
    - Grain-atom sputtering
    - UV photo-dissociation
- HPC Simulations, increased complexity, increased range

# Continuation of Work
-

# Conclusion
