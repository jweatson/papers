# Literature Review
While the previous section covers the background subject matter, this section will cover two papers of particular importance in depth; the first, _Pinwheels in the Sky, With Dust_ [@hendrix_pinwheels_2016] follows a similar premise to this project albeit with some differences. _Radiation Driven Winds in Of Stars_ [@castor_radiation-driven_1975] describes the CAK theory, the seminal paper for [radiative line driving](#driving-forces), which should be analysed in more detail in order to better understand the underlying physics behind wind propagation.

<!--  convert the hyperlink here to a /ref /label style  -->

<!-- A third paper should be used, if time allows, however, maybe use a different paper with a similar bent to mine, if one can be found  -->

## Pinwheels in the Sky, With Dust
_Pinwheels in the Sky_ is a paper with a very similar premise to this project, where a numerical simulation is used with the explicit purpose of modelling the dust dynamics of the system WR 98a, to determine which properties lead to its distinctive spiral structure. The main conclusion of this paper was that cooling due to emission from the wind plasma was not a key factor in the morphology of the wind collision region, hence the system obeys adiabatic conditions. Furthermore the presence of strong cooling causes thermal instabilities to dominate along the region.

### Research & Conclusions
The outcome of this paper is significantly different to this project, instead focusing on the formation of the spiral structure of WR 98a and using dust for synthetic imaging to compare to archival observational data, and to determine if the features would be detectable with future telescopes. WR 98a is described as a prime target for interferometric surveys, due to a variety of factors such as its large, spiral dust lane which emits strongly in the infrared, in addition with in inclination of $35^\circ$ to the line of sight, it can be well observed from ground stations on Earth; WR 98a in addition has a short orbital period of $1.4~\text{yr}$, meaning that multiple orbital passes can be observed within a short time period, hence its dust forming periods can be determined. This is not the case for some periodic dust forming CWB systems, and is crucial for simulating periodic dust formation in the simulations performed in this paper. Tables \ref{wr98atable} and \ref{wr98atable2} contains system parameters and parameters for each star.

<!--  Provide examples of periodic CWB systems and their orbital periods, perhaps do this in an earlier section, maybe the wolf rayet section? -->

: Properties of WR and OB star in WR 98a \label{wr98atable}

Parameter  | WR                                                 | OB
-----------|----------------------------------------------------|---------------------------------------------------
$M$        | $10 ~\text M_\odot$                                | $18 ~\text M_\odot$
$\dot M$   | $0.5\times 10^{-5} ~\text M_\odot \text{ yr}^{-1}$ | $0.5\times 10^{-7} ~\text M_\odot \text{ yr}^{-1}$
$v_\infty$ | $900 ~\text{km}\text{ s}^{-1}$                     | $2000 ~\text{km}\text{ s}^{-1}$

: Properties of WR 98a system \label{wr98atable2}

Parameter         | Value
------------------|-------------------------------------------------------
$a$               | $6.08\times 10^{13} ~\text{cm} \approx 4.06~\text{au}$
$P_\text{orb}$    | $4.89\times 10^7 ~\text{s} \approx 1.55~\text{yr}$
$\rho_\text{ISM}$ | $10^{-20}~\text{g cm}^{-3}$

<!--  These captions should be fixed up in the latex document -->

Similar to this project, an AMR code is used to simulate the colliding wind binary system, as it is the only feasible, efficient method of simulating this type of system, due to the small scale of the wind collision region and the large overall system size. The size of the simulation is $(320 \times 320 \times 140)a$, or approximately $(1200\times 1200 \times 568) ~\text{au}$; this is a fairly compact system, but is capable of simulating the wind collision region and the spiral structure. The AMR simulation has 11 levels, leading to an effective resolution of $(81920 \times 81920 \times 24576)$, this is significantly higher resolution than any simulation yet attempted in this project, which is reasonable considering the projects overall progress; however the number of levels is interesting to note, and would uniquely benefit the simulation of colliding wind binary systems, due to their extremely small finest feature sizes.

The way that dust is handled in this paper is significantly different from this project, providing insight into how the code should progress beyond the initial version; as such section /ref{dustcomphendrix} deconstructs the method behind the [@hendrix_pinwheels_2016] dust code prescription.

![Keck I high angular resolution multiwavelength image of the pinwheel nebula WR 98a [@hendrix_pinwheels_2016] \label{wr98ahendrixkeck}](assets/Hendrix/stw1289fig1.jpeg)

![Synthetic images representative of WR 98a without cooling, this is similar to observational data recorded by the Keck telescope [@hendrix_pinwheels_2016] \label{hendrixnocooling}](assets/Hendrix/stw1289fig12.jpeg)


Monte Carlo dusty radiative transfer is used to generate a series of synthetic images in the infrared, as well as to perform virtual photometry. This is performed in order to compare the simulations with observational data, as well as to simulate the performance of more modern telescopes when observational data from them was available. Figure \ref{hendrixnocooling} demonstrates these virtual images, and how emission wavelength and angular resolution can be adjusted.

The resultant data from this process shows that there is a correlation between the adiabatic results from figure \ref{hendrixnocooling}, and the observational results from the Keck telescope (figure \ref{wr98ahendrixkeck}); when the system is radiative, thermal instabilities along the wind collision region disrupt its notable spiral shape, and lead to a more diffuse emission. While the Keck telescope data is not of particularly high resolution, at the time of writing there was no observational data from more contemporary telescopes, such as the E-ELT or ALMA; if higher resolution observational data becomes available during the span of this project, this could provide a way to expand on this project, by revisiting some of the aspects of this paper with better observational data and a more comprehensive dust model.

![Synthetic images representative of WR 98a with strong cooling, there is a significant disruption of the wind collision region due to thermal instabilities, as such it does not match observational data [@hendrix_pinwheels_2016] \label{hendrixcooling}](assets/Hendrix/stw1289fig14.jpeg)

### Comparison of Dust Mechanisms \label{dustcomphendrix}
Since this paper is one of the few papers that models dust within a colliding wind binary system, a comparison of how dust is modelled between this paper and the project is in order. Of important are the underlying parameters, as well as the mechanism behind creation, propagation and destruction of dust; in addition to this, its effect on the environment, such as through cooling are also considered. The paper covers most of these, using significantly different methods compared to this project, while not including some of the routines for computational simplicity and a lack of necessity.

#### Creation and Properties
Dust is created within the shock at a rate determined by observational work on the star system attempting to be simulated, it is tied to an empirical law, and is not based on the physical properties of the system; furthermore dust is only created below a threshold orbital separation to emulate the periodic nature of dust formation in most observed systems. This can be justified by [@williams_infrared_1987], which suggests that in multiple classes of WC systems exhibiting periodicity only a minor change in density affects whether dust is formed or not, causing the systems to behave as if they are in a bistable equilibrium.

Dust grain sizes is based on an average of a distribution of dust grains from [@yudin_speckle_2001], where the average grain radius $\bar a_d = 15.44\times 10^{-7} \text{cm}$. While a grain size distribution could be implemented, this would be significantly more computationally complex, and would be unnecessary when factoring in the goals of the project.

Currently, the multiple scalar method of this project can allow for an arbitrary number of grain sizes, all modelled as separate scalars; these scalars can be modified to simulate varying grain sizes, dust mass fraction $z$ and bulk densities. This work is expanded upon in section \ref{multiplescalarext}.

#### Propagation and simulation
Hendrix et al. utilise a multi-fluid method of modelling dust in their simulation, having it act as a separate fluid to the stellar wind that is coupled to the wind with the Epstein drag force

$$
f_d = (1-\alpha(T)) \sqrt \frac{8\gamma p}{\pi \rho} \frac{\rho \rho_d}{\rho_p a_d} (\boldsymbol v_d - \boldsymbol v) ~,
$$

where $\rho_d$ is the dust fluid density, $\rho_p$ is the grain bulk density, $a_d$ is the grain size and $\alpha$ is the "sticking coefficient" denoting the momentum transfer between dust and gas, defined as

$$
\alpha = 0.35\exp\left( - \sqrt \frac{T T_0}{500~\text K} \right) +0.1 ~,
$$

This allows for the accurate modelling of the dynamics of larger grains, which would require significantly more momentum to accelerate; however this is a computationally expensive method due to the modelling of an entirely separate fluid, and calculating a drag force that may be unnecessary considering the conditions of the environment, where the harsh environment would prevent large, heavy dust grains from forming.

<!--  sentence is a little overly long -->

At the moment, this project utilises an advected scalar method, where the dust properties are a conserved quantity of the fluid and propagate alongside it; this is physically accurate for small dust grains, in the order of $10^{-3}\text - 10^{-2}~\mu \text m$ while significantly reduces computational complexity and memory requirements, this can also easily allow the modelling of multiple dust grains by simply iterating the calculations over the number of scalars.

<!-- might make sense to clear this up a little bit, or talk about advected scalars earlier in the project than the next chapter  -->

<!-- is there a better way of doing dashes in latex math? -->

#### Routines not included
<!--  Change wording of this title -->
As the focus of this paper was on the circumstances leading to the spiral structure of the WR98a wind collision region, many dust behaviours were not included or considered, as they would have a negligible impact on the wind structure.

<!--  circumstances? it's? -->

Dust cooling is not included in this project, and is instead used for synthetic observation, in particular synthetic infrared imaging; instead, plasma cooling is the only considered cooling phenomena. While including dust cooling would improve the accuracy of the simulation, the results of the paper indicated that the system was strongly adiabatic, meaning that dust cooling did not need to be considered.

As with cooling, there are no routines for dust destruction, it is assumed that during periods where dust is being created, the amount that is created far exceeds the amount being destroyed; given the violent conditions of a colliding wind binary system and its conduciveness to dust destruction. However, since the dust production rate is calculated from observational data, this would of course include the final amount of dust being produced, which may result in a different distribution of dust throughout the system, but would still be fairly accurate.

### Overall Comparison With Project
