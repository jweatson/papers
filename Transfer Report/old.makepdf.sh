pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--table-of-contents \
--columns=2 \
--metadata link-citations \
--variable urlcolor=cyan \
-o transfer-report.pdf transfer-report.md

echo "Finished PDF rendering"

pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--table-of-contents \
--columns=2 \
--metadata link-citations \
--variable urlcolor=cyan \
--mathjax \
-o web/transfer-report.html transfer-report.md

echo "Finished HTML rendering"

bash writecomments.sh
