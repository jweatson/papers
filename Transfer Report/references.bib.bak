@article{crowther2007physical,
  title={Physical properties of Wolf-Rayet stars},
  author={Crowther, Paul A},
  journal={Annu. Rev. Astron. Astrophys.},
  volume={45},
  pages={177--219},
  year={2007},
  publisher={Annual Reviews}
}

DUST



@article{strickland1995numerical,
  title={Numerical analysis of the dynamic stability of radiative shocks},
  author={Strickland, Russell and Blondin, John M},
  journal={The Astrophysical Journal},
  volume={449},
  pages={727},
  year={1995}
}

@article{draine1979destruction,
  title={Destruction mechanisms for interstellar dust},
  author={Draine, BT and Salpeter, EE},
  journal={The Astrophysical Journal},
  volume={231},
  pages={438--455},
  year={1979}
}


STELLAR WINDS

@book{lamers1999introduction,
  title={Introduction to stellar winds},
  author={Lamers, Henny JGLM and Cassinelli, Joseph P},
  year={1999},
  publisher={Cambridge university press}
}

RADIATIVE LINE DRIVING

@article{gayley1997sudden,
  title={Sudden radiative braking in colliding hot-star winds},
  author={Gayley, KG and Owocki, SP and Cranmer, SR},
  journal={The Astrophysical Journal},
  volume={475},
  number={2},
  pages={786},
  year={1997},
  publisher={IOP Publishing}
}

@article{stevens1994stagnation,
  title={Stagnation-point flow in colliding-wind binary systems},
  author={Stevens, Ian R and Pollock, AMT},
  journal={Monthly Notices of the Royal Astronomical Society},
  volume={269},
  number={2},
  pages={226--234},
  year={1994},
  publisher={Oxford University Press Oxford, UK}
}

NUMERICAL SIMULATION

@book{toro2013riemann,
  title={Riemann solvers and numerical methods for fluid dynamics: a practical introduction},
  author={Toro, Eleuterio F},
  year={2013},
  publisher={Springer Science \& Business Media}
}

@book{wareing2005hydrodynamical,
  title={Hydrodynamical modelling of planetary nebulae and their interaction with the interstellar medium},
  author={Wareing, Christopher J},
  year={2005}
}

COLLIDING WINDS

@article{stevens1992colliding,
  title={Colliding winds from early-type stars in binary systems},
  author={Stevens, Ian R and Blondin, John M and Pollock, AMT},
  journal={The Astrophysical Journal},
  volume={386},
  pages={265--287},
  year={1992}
}
