<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <meta name="author" content="Joseph Eatson" />
  <title>Transfer Report</title>
  <style type="text/css">code{white-space: pre;}</style>
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>
</head>
<body>
<div id="header">
<h1 class="title">Transfer Report</h1>
<h2 class="author">Joseph Eatson</h2>
<h3 class="date">May-August 2018</h3>
</div>
<div id="TOC">
<ul>
<li><a href="#summary">Summary</a></li>
<li><a href="#background">Background</a><ul>
<li><a href="#stellar-winds">Stellar Winds</a><ul>
<li><a href="#driving-forces">Driving Forces</a></li>
<li><a href="#seeking-an-effective-model-of-radiative-line-driving">Seeking an Effective Model of Radiative Line Driving</a></li>
</ul></li>
<li><a href="#massive-binary-star-systems">Massive Binary Star Systems</a><ul>
<li><a href="#wolf-rayet-stars">Wolf-Rayet Stars</a></li>
</ul></li>
<li><a href="#computational-astrophysics">Computational Astrophysics</a><ul>
<li><a href="#numerical-simulation">Numerical Simulation</a></li>
<li><a href="#radiative-transfer">Radiative Transfer</a></li>
</ul></li>
</ul></li>
<li><a href="#literature-review">Literature Review</a><ul>
<li><a href="#pinwheels-in-the-sky-with-dust">Pinwheels in the Sky, With Dust</a><ul>
<li><a href="#research-conclusions">Research &amp; Conclusions</a></li>
<li><a href="#comparison-of-dust-mechanisms">Comparison of Dust Mechanisms </a></li>
<li><a href="#overall-comparison-with-project">Overall Comparison With Project</a></li>
</ul></li>
</ul></li>
<li><a href="#project-outline">Project Outline</a><ul>
<li><a href="#towards-a-comprehensive-dust-model">Towards a Comprehensive Dust Model</a></li>
<li><a href="#simulation-of-colliding-wind-binary-systems">Simulation of Colliding Wind Binary Systems</a></li>
<li><a href="#comparison-with-observational-data">Comparison with Observational Data</a></li>
</ul></li>
<li><a href="#progress">Progress</a><ul>
<li><a href="#dust-modelling">Dust Modelling</a><ul>
<li><a href="#multiple-scalar-model">Multiple Scalar Model</a></li>
</ul></li>
</ul></li>
<li><a href="#progression-beyond-the-first-year">Progression Beyond The First Year</a></li>
<li><a href="#appendix">Appendix</a></li>
<li><a href="#references">References</a></li>
</ul>
</div>
<h1 id="summary">Summary</h1>
<p>It is generally agreed upon that the bulk of dust formation in the galaxy occurs inside star systems containing Asymptotic Giant Branch (AGB) stars. However, there are additional formation processes that produce a non-insignificant amount of interstellar dust. One such process is dust production in Wind Collision Regions (WCR’s).</p>
<p>Wind Collision Regions form when the winds of two massive stars in a binary pair collide. The resultant shock heats the surrounding environment to temperatures exceeding <span class="math inline">\(10^8 \text K\)</span>. An area of particular interest is where the winds collide head on, producing the most energetic conditions in the system, however this area is extremely compact, occurring over an area smaller than <span class="math inline">\(1 \text{AU}\)</span>, this compactness, and the relative distance of nearest CWB systems excludes direct observation, meaning that numerical hydrodynamic simulation must be used.</p>
<p>The goal of this project is to simulate the conditions inside a CWB system, in particular the dust properties of the system; this transfer report will detail the physical phenomena occurring within the system, an analysis of contemporary research into these systems, the current state of the project as of the first year, and future work, both immediate and long-term.</p>
<h1 id="background">Background</h1>
<h2 id="stellar-winds">Stellar Winds</h2>
<p>Since the turn of the century, stellar winds have been observed by astronomers, and since the discovery of emission lines, spectroscopic data has been obtained for them. Doppler shifts of over 100 kilometres per second were viewed with scientific curiosity, and early photographic observations noted the slowly increasing angular diameter of the clouds - the idea of the universe as static and unchanging was being rapidly considered.</p>
<p>By the 1940’s, early mathematical models of these systems were being considered, however Underhill noted that radiation pressure alone was not sufficient to account for the sheer amount of material being ejected from OB stars - something was afoot, just out of sight.</p>
<p>In the second half of the century telescopes capable of seeing beyond visible light were built, opening up new avenues of observation; these new observatories showed us the infrared excesses of dust emission, and the strong X-ray emission of interstellar shocks - this comprehensive view of interstellar space allowed for the answering of long-standing questions.</p>
<h3 id="driving-forces">Driving Forces</h3>
<p>The primary radiative driving mechanism of low-mass stars is Thompson scattering, a form of elastic scattering where the photon interacts with a charged particle, changing the trajectory and velocity of the particle. This interaction is entirely dependent on the Thomson cross section, <span class="math inline">\(\sigma_t\)</span>, which helps determine the likelihood of a collision between a photon and a charged particle; in the case of an electron</p>
<p><span class="math display">\[
\sigma_t = \frac{8\pi}{3} \left( \frac{\alpha \hbar}{mc}\right)^2 \approx 6.652458 \times 10^{-29} \text m^2 ~~,
\]</span> <!-- perhaps include another equation to show the interaction probability of Thomson scattering --></p>
<p>where <span class="math inline">\(\alpha\)</span> is the fine structure constant, <span class="math inline">\(1/137\)</span>. The cross section of an electron is extremely small, and even in the comparatively dense regime of a stellar wind, the opacity is not sufficient to drive fast, dense winds. For AGB stars, their low surface gravity results in the driving of extremely dense but slow winds. But for massive stars, capable of driving away the same amount of material but at speeds exceeding <span class="math inline">\(1000 \text{ km/s}\)</span>, there has to be a mechanism with a significantly greater opacity in order to drive the wind at the observed velocities and loss rates, while counteracting the significantly stronger surface gravity.</p>
<table>
<thead>
<tr class="header">
<th>Star</th>
<th><span class="math inline">\(v_\infty~(\text{km s}^{-1})\)</span></th>
<th><span class="math inline">\(\dot M~(\text{M}_\odot~\text{yr}^{-1})\)</span></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Solar mass star</td>
<td><span class="math inline">\(400-700\)</span></td>
<td><span class="math inline">\(10^{-14}\)</span></td>
</tr>
<tr class="even">
<td>AGB star</td>
<td><span class="math inline">\(10-30\)</span></td>
<td><span class="math inline">\(10^{-5}\)</span></td>
</tr>
<tr class="odd">
<td>OB star</td>
<td>1000-3000</td>
<td><span class="math inline">\(10^{-6} - 10^{-5}\)</span></td>
</tr>
</tbody>
</table>
<!-- Make this table more elaborate, add additional details, and wolf rayet stars to it -->
<p>Instead, massive stars utilise radiative line driving as the motive force behind their impressive winds. Hot stars emit large amounts of UV radiation at a variety of frequencies, some of which correspond with resonance lines of the elements in the wind; these resonance lines have an opacity <span class="math inline">\(10^6\)</span> times greater than Thomson scattering <span class="citation">(Lamers and Cassinelli <a href="#ref-lamers_introduction_1999">1999</a>)</span>; the rest of the wind is then accelerated with the ions via <em>Coulomb coupling</em>. Due to the limited number of resonant lines the motive force would be weaker than Thomson scattering if not for Doppler shift; as the wind velocity increases the photons are Doppler shifted in the reference frame of the atoms in the wind, resulting in a greater number of absorption events, and a greater motive force. The result of this phenomenon is increasing acceleration as the wind speeds increase (up to its terminal velocity), resulting in increased acceleration as the wind travels further from the star - the inverse of the Thomson scattering acceleration curve.</p>
<!-- Perhaps find a graph supporting this? -->
<p>The <em>mg</em> hydrodynamics code used in this project supports radiative driving, in addition to its default of using instantaneous acceleration. As the acceleration of the wind is gradual, reaching a terminal velocity at some distance from the star. For a binary system with a small separation distance this can result in a significant deviation in the structure of the collision; this is compounded by the effect of sudden radiative braking and stagnation-point flow, where radiative line driving from the secondary star acts on the flow from the primary star, rapidly braking it - these effects are further detailed in <span class="citation">(Stevens and Pollock <a href="#ref-stevens_stagnation-point_1994">1994</a>)</span> and <span class="citation">(Gayley, Owocki, and Cranmer <a href="#ref-gayley_sudden_1997">1997</a>)</span>.</p>
<!-- verify this -->
<h3 id="seeking-an-effective-model-of-radiative-line-driving">Seeking an Effective Model of Radiative Line Driving</h3>
<p>As previously discussed, line driving is the mechanism of choice for acceleration of dense, fast winds around massive stars; however, a highly accurate simulation capable of modelling every single line, including its scattering potential, and the path it takes through the wind , would fall under the purview of ray-tracing for every single time step of a simulation, something that is not computationally feasible.</p>
<p>Parameterising the equations reduces computational complexity, and allows this to be calculated at every step in a hydrodynamical simulation effectively; however, careful consideration must be made regarding phenomena, in order for it to be effectively modelled. This is also a good example of a core tenant of effective numerical simulation, the trade-off between physical accuracy and simulation complexity.</p>
<!--  This is very much a placeholder paragraph, especially regarding parameterisation -->
<h4 id="the-cak-formalism">The CAK Formalism</h4>
<p>The CAK formalism on the other hand seeks to parametrise the force exerted on stellar wind material as a whole,</p>
<p>With these models, there exist critical points where line driving is maximised due to</p>
<p>This formalism forms the basis behind all radiatively driven wind models for massive stars,</p>
<h4 id="the-sobolev-approximation">The Sobolev Approximation</h4>
<h4 id="the-finite-disk-correction-factor">The Finite Disk Correction Factor</h4>
<p>A final modification to the CAK formalism is considering the effect of proximity to the star has on the photon flux received by the stellar material, if the star is assumed to be a uniformly radiating surface, then the photon flux decreases at close proximity.</p>
<p>At a distance however, assuming that each photon performs a single scattering event, there are more available photons, leading to an increase in flux - and an increase in force; combined with the decreased force due to gravity from the star, this leads to a significant increase in final velocity, explaining discrepancies from the earlier model.</p>
<p>The finite disk correction factor is defined as</p>
<p><span class="math display">\[
f = \frac{(1+\sigma)^{1+\alpha} - (1+\sigma \mu_c^2)^{1+\alpha}}
{(1+\alpha)(1-\mu_c^2)\sigma(1+\sigma)^\alpha}~,
\]</span></p>
<p>where <span class="math inline">\(\sigma = d\ln(v/d)\ln(r-1)\)</span>, <span class="math inline">\(\mu^2_c = 1-(R^2/r^2)\)</span> and <span class="math inline">\(\alpha\)</span> is the CAK parameter characterising the distribution of optically thick optically thin lines <span class="citation">(Friend and Abbott <a href="#ref-friend_theory_1986">1986</a>)</span>.</p>
<h2 id="massive-binary-star-systems">Massive Binary Star Systems</h2>
<h3 id="wolf-rayet-stars">Wolf-Rayet Stars</h3>
<p>Wolf-Rayet stars, discovered by Charles Wolf and Georges Rayet in 1867, are characterised by their extremely broad emission lines; the classification can be further subdivided into WN, WC and WO subtypes through the presence of a strong secondary nitrogen, carbon or oxygen line.</p>
<p>The reason for these strong emission lines is due to the</p>
<h2 id="computational-astrophysics">Computational Astrophysics</h2>
<p>Due to the small scale of the system and the comparative distance of CWB systems, most analysis of these systems is done using simulations, which are then compared to the scarce available observational data. Typically the wind structure of the system is performed with <a href="#numerical-simulation">numerical simulation</a> and the fluid <!-- is this a good word for it? --> properties of the system are used to derive observable properties using <a href="#radiative-transfer">radiative transfer</a></p>
<h3 id="numerical-simulation">Numerical Simulation</h3>
<p>Inviscid, compressible flows, such as the movement of atoms and molecules through the interstellar medium are governed by the Euler equations, which in vector form are</p>
<p><span class="math display">\[
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
\]</span></p>
<p><span class="math display">\[
\frac{\partial \rho}{\partial t} + \nabla \cdot (p\boldsymbol u) = 0 ~~,
\]</span></p>
<p><span class="math display">\[
\frac{\partial \rho \boldsymbol u}{\partial t} + \nabla \cdot (\rho\boldsymbol u u + P) = \rho \boldsymbol f ~~,
\]</span></p>
<p>where <span class="math inline">\(\gamma\)</span> is the ratio of specific heats - typically <span class="math inline">\(5/3\)</span>, <span class="math inline">\(\epsilon = \boldsymbol u^2 / 2 + e/\rho\)</span>, the total internal energy, <span class="math inline">\(e = P/(\gamma-1)\)</span> is the internal energy density and <span class="math inline">\(\boldsymbol f\)</span> is the Cartesian force vector. In each of the Euler equations the first term refers to a conserved quantity, while the second term refers to a flux vector <span class="math inline">\(\textbf F\)</span> - leading to the conservative form</p>
<!--  Clear this up!!! -->
<!--  is it sensible to use \vec? perhaps change when converting to pdf  -->
<p><span class="math display">\[
\frac{\partial q}{\partial t} + \nabla \cdot \textbf F = 0 ~~,
\]</span></p>
<p>where the flux vector is a function of the conserved quantities</p>
<p><span class="math display">\[
\textbf F =
\begin{pmatrix}
\rho \textbf u \\
p + \rho \textbf u u \\
\textbf u (E+p)
\end{pmatrix}
~,~
q =
\begin{pmatrix}
\rho \\
\rho \textbf u \\
E
\end{pmatrix} ~~,
\]</span></p>
<p>The Euler equations are coupled nonlinear partial differential equations, hence no general solutions exist; as such, time dependent numerical solutions must be used instead. Numerical solutions in the simplest terms take the problem at its origin,</p>
<p>This is a flexible way of solving almost any problem involving coupled differential equations, albeit at the cost of dramatically increased CPU time and memory usage.</p>
<h4 id="the-godunov-approach">The Godunov Approach</h4>
<p>The simplest method of solving the Euler equations in the hyperbolic case is through the Godunov approach,</p>
<p>The Riemann problem represents a discontinuity in a fluid, with two sets of conserved variables and an instantaneous change between then. In the hyperbolic case, the initial conditions can be described thusly</p>
<!--  word better -->
<p><span class="math display">\[
\left.
\begin{array}{rl}
  \text{PDEs:} &amp; \frac{\partial \textbf U}{\partial t} + \textbf{A} \frac{\partial \textbf U}{\partial x} = 0 ~,~ - \infty &lt; x&lt; \infty ~,~ t &gt; 0 ~, \\
  \text{Initial Conditions: } &amp; \textbf{U}(x,0) = \textbf{U}^{(0)} = \left\{
  \begin{array}{l}
    \textbf U_{\text L} ~ x &lt; 0 ~,~ \\
    \textbf U_{\text R} ~ x &gt; 0
  \end{array}
  \right.
\end{array} \right\}
\]</span></p>
<p>where <span class="math inline">\(\textbf U\)</span> is a vector of conserved variables, such as velocity, pressure and density <span class="citation">(Toro <a href="#ref-toro_riemann_2013">2013</a>)</span>. This closely mimics the Rankine-Huginot jump conditions describing a shock. This can be seen in figure </p>
<div class="figure">
<img src="assets/Riemann.png" alt="The initial conditions of a riemann problem, where \textbf U is a conserved variable " />
<p class="caption">The initial conditions of a riemann problem, where <span class="math inline">\(\textbf U\)</span> is a conserved variable </p>
</div>
<!--  check this for accuracy, perhaps simplify? ask julian regarding this one -->
<p>When broken into cells in a piecewise-constant fashion, a flow is analogous to a series of Riemann problems; Godunov’s scheme does exactly this, approximating the solution to the Euler equations via this series. As such, solving the Euler equations in multiple dimensions involves solving the Riemann problems for each face of a grid cell <span class="citation">(Wareing <a href="#ref-wareing_hydrodynamical_2005">2005</a>)</span>. Piecewise constant is the simplest form that Godunov’s scheme can take, however higher-order extensions are typically used instead, by interpolating cells using a piecewise linear or piecewise parabolic method.</p>
<!-- Read more of Wareings paper, and the Godunov paper cited by Julians thesis  -->
<!--  The Rankine-Huginot shock jump conditions can be used to find the exact solution of the Riemann problem -->
<h4 id="adaptive-mesh-refinement">Adaptive Mesh Refinement</h4>
<p>A particular problem with numerical simulations was the sheer difficulty of solving 3-dimensional problems; this is due to the exponential growth of cells required as dimensionality is increased. While colliding winds can be assumed to be cylindrically symmetric in the most basic cases, introducing orbital motion will destroy axisymmetry due to the Coriolis force, mandating 3-dimensional hydrodynamics for any simulation that evolves as a function of time. In the specific case of CWB systems, the problem is compounded, due to the large variance in length scales, from sub-AU in the WCR to the parsec scale as the shock propagates out of the system. Adaptive Mesh Refinement (AMR) sidesteps this conundrum by changing the effective grid resolution as the simulatengad ion runs, with an n-dimensional cell “refining” into <span class="math inline">\(2^n\)</span> cells when needed; the condition for this refinement is if the Riemann solutions for the finest cell and its “parent” cell are in disagreement.</p>
<div class="figure">
<img src="assets/amr.png" alt="An example of AMR, more complex regions of the simulation are refined, saving significant amounts of computational time and memory" />
<p class="caption">An example of AMR, more “complex” regions of the simulation are refined, saving significant amounts of computational time and memory</p>
</div>
<!-- Use a more up to date image of the grid array, perhaps make one in python? -->
<p>In the structure of <em>mg</em>, a “join” array is used to connect cells and levels together, meaning that there is a degree of CPU and memory overhead; this is on top of the overhead of comparing fine cells with their parent cells. However,5 this overhead is negligible compared to computing all cells without AMR.</p>
<!-- this may be a little clumsy an explanation? -->
<!-- Introduce a benchmark of some kind? For AMR vs non? -->
<h3 id="radiative-transfer">Radiative Transfer</h3>
<h1 id="literature-review">Literature Review</h1>
<p>While the previous section covers the background subject matter, this section will cover two papers of particular importance in depth; the first, <em>Pinwheels in the Sky, With Dust</em> <span class="citation">(Hendrix et al. <a href="#ref-hendrix_pinwheels_2016">2016</a>)</span> follows a similar premise to this project albeit with some differences. <em>Radiation Driven Winds in Of Stars</em> <span class="citation">(Castor, Abbott, and Klein <a href="#ref-castor_radiation-driven_1975">1975</a>)</span> describes the CAK theory, the seminal paper for <a href="#driving-forces">radiative line driving</a>, which should be analysed in more detail in order to better understand the underlying physics behind wind propagation.</p>
<!--  convert the hyperlink here to a /ref /label style  -->
<!-- A third paper should be used, if time allows, however, maybe use a different paper with a similar bent to mine, if one can be found  -->
<h2 id="pinwheels-in-the-sky-with-dust">Pinwheels in the Sky, With Dust</h2>
<p><em>Pinwheels in the Sky</em> is a paper with a very similar premise to this project, where a numerical simulation is used with the explicit purpose of modelling the dust dynamics of the system WR 98a, to determine which properties lead to its distinctive spiral structure. The main conclusion of this paper was that cooling due to emission from the wind plasma was not a key factor in the morphology of the wind collision region, hence the system obeys adiabatic conditions. Furthermore the presence of strong cooling causes thermal instabilities to dominate along the region.</p>
<h3 id="research-conclusions">Research &amp; Conclusions</h3>
<p>The outcome of this paper is significantly different to this project, instead focusing on the formation of the spiral structure of WR 98a and using dust for synthetic imaging to compare to archival observational data, and to determine if the features would be detectable with future telescopes. WR 98a is described as a prime target for interferometric surveys, due to a variety of factors such as its large, spiral dust lane which emits strongly in the infrared, in addition with in inclination of <span class="math inline">\(35^\circ\)</span> to the line of sight, it can be well observed from ground stations on Earth; WR 98a in addition has a short orbital period of <span class="math inline">\(1.4~\text{yr}\)</span>, meaning that multiple orbital passes can be observed within a short time period, hence its dust forming periods can be determined. This is not the case for some periodic dust forming CWB systems, and is crucial for simulating periodic dust formation in the simulations performed in this paper. Tables  and  contains system parameters and parameters for each star.</p>
<!--  Provide examples of periodic CWB systems and their orbital periods, perhaps do this in an earlier section, maybe the wolf rayet section? -->
<table>
<caption>Properties of WR and OB star in WR 98a </caption>
<colgroup>
<col width="10%" />
<col width="45%" />
<col width="44%" />
</colgroup>
<thead>
<tr class="header">
<th>Parameter</th>
<th>WR</th>
<th>OB</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><span class="math inline">\(M\)</span></td>
<td><span class="math inline">\(10 ~\text M_\odot\)</span></td>
<td><span class="math inline">\(18 ~\text M_\odot\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(\dot M\)</span></td>
<td><span class="math inline">\(0.5\times 10^{-5} ~\text M_\odot \text{ yr}^{-1}\)</span></td>
<td><span class="math inline">\(0.5\times 10^{-7} ~\text M_\odot \text{ yr}^{-1}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(v_\infty\)</span></td>
<td><span class="math inline">\(900 ~\text{km}\text{ s}^{-1}\)</span></td>
<td><span class="math inline">\(2000 ~\text{km}\text{ s}^{-1}\)</span></td>
</tr>
</tbody>
</table>
<table>
<caption>Properties of WR 98a system </caption>
<thead>
<tr class="header">
<th>Parameter</th>
<th>Value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><span class="math inline">\(a\)</span></td>
<td><span class="math inline">\(6.08\times 10^{13} ~\text{cm} \approx 4.06~\text{au}\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(P_\text{orb}\)</span></td>
<td><span class="math inline">\(4.89\times 10^7 ~\text{s} \approx 1.55~\text{yr}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(\rho_\text{ISM}\)</span></td>
<td><span class="math inline">\(10^{-20}~\text{g cm}^{-3}\)</span></td>
</tr>
</tbody>
</table>
<!--  These captions should be fixed up in the latex document -->
<p>Similar to this project, an AMR code is used to simulate the colliding wind binary system, as it is the only feasible, efficient method of simulating this type of system, due to the small scale of the wind collision region and the large overall system size. The size of the simulation is <span class="math inline">\((320 \times 320 \times 140)a\)</span>, or approximately <span class="math inline">\((1200\times 1200 \times 568) ~\text{au}\)</span>; this is a fairly compact system, but is capable of simulating the wind collision region and the spiral structure. The AMR simulation has 11 levels, leading to an effective resolution of <span class="math inline">\((81920 \times 81920 \times 24576)\)</span>, this is significantly higher resolution than any simulation yet attempted in this project, which is reasonable considering the projects overall progress; however the number of levels is interesting to note, and would uniquely benefit the simulation of colliding wind binary systems, due to their extremely small finest feature sizes.</p>
<p>The way that dust is handled in this paper is significantly different from this project, providing insight into how the code should progress beyond the initial version; as such section /ref{dustcomphendrix} deconstructs the method behind the <span class="citation">(Hendrix et al. <a href="#ref-hendrix_pinwheels_2016">2016</a>)</span> dust code prescription.</p>
<div class="figure">
<img src="assets/Hendrix/stw1289fig1.jpeg" alt="Keck I high angular resolution multiwavelength image of the pinwheel nebula WR 98a (Hendrix et al. 2016) " />
<p class="caption">Keck I high angular resolution multiwavelength image of the pinwheel nebula WR 98a <span class="citation">(Hendrix et al. <a href="#ref-hendrix_pinwheels_2016">2016</a>)</span> </p>
</div>
<div class="figure">
<img src="assets/Hendrix/stw1289fig12.jpeg" alt="Synthetic images representative of WR 98a without cooling, this is similar to observational data recorded by the Keck telescope (Hendrix et al. 2016) " />
<p class="caption">Synthetic images representative of WR 98a without cooling, this is similar to observational data recorded by the Keck telescope <span class="citation">(Hendrix et al. <a href="#ref-hendrix_pinwheels_2016">2016</a>)</span> </p>
</div>
<p>Monte Carlo dusty radiative transfer is used to generate a series of synthetic images in the infrared, as well as to perform virtual photometry. This is performed in order to compare the simulations with observational data, as well as to simulate the performance of more modern telescopes when observational data from them was available. Figure  demonstrates these virtual images, and how emission wavelength and angular resolution can be adjusted.</p>
<p>The resultant data from this process shows that there is a correlation between the adiabatic results from figure , and the observational results from the Keck telescope (figure ); when the system is radiative, thermal instabilities along the wind collision region disrupt its notable spiral shape, and lead to a more diffuse emission. While the Keck telescope data is not of particularly high resolution, at the time of writing there was no observational data from more contemporary telescopes, such as the E-ELT or ALMA; if higher resolution observational data becomes available during the span of this project, this could provide a way to expand on this project, by revisiting some of the aspects of this paper with better observational data and a more comprehensive dust model.</p>
<div class="figure">
<img src="assets/Hendrix/stw1289fig14.jpeg" alt="Synthetic images representative of WR 98a with strong cooling, there is a significant disruption of the wind collision region due to thermal instabilities, as such it does not match observational data (Hendrix et al. 2016) " />
<p class="caption">Synthetic images representative of WR 98a with strong cooling, there is a significant disruption of the wind collision region due to thermal instabilities, as such it does not match observational data <span class="citation">(Hendrix et al. <a href="#ref-hendrix_pinwheels_2016">2016</a>)</span> </p>
</div>
<h3 id="comparison-of-dust-mechanisms">Comparison of Dust Mechanisms </h3>
<p>Since this paper is one of the few papers that models dust within a colliding wind binary system, a comparison of how dust is modelled between this paper and the project is in order. Of important are the underlying parameters, as well as the mechanism behind creation, propagation and destruction of dust; in addition to this, its effect on the environment, such as through cooling are also considered. The paper covers most of these, using significantly different methods compared to this project, while not including some of the routines for computational simplicity and a lack of necessity.</p>
<h4 id="creation-and-properties">Creation and Properties</h4>
<p>Dust is created within the shock at a rate determined by observational work on the star system attempting to be simulated, it is tied to an empirical law, and is not based on the physical properties of the system; furthermore dust is only created below a threshold orbital separation to emulate the periodic nature of dust formation in most observed systems. This can be justified by <span class="citation">(Williams, van der Hucht, and The <a href="#ref-williams_infrared_1987">1987</a>)</span>, which suggests that in multiple classes of WC systems exhibiting periodicity only a minor change in density affects whether dust is formed or not, causing the systems to behave as if they are in a bistable equilibrium.</p>
<p>Dust grain sizes is based on an average of a distribution of dust grains from <span class="citation">(Yudin et al. <a href="#ref-yudin_speckle_2001">2001</a>)</span>, where the average grain radius <span class="math inline">\(\bar a_d = 15.44\times 10^{-7} \text{cm}\)</span>. While a grain size distribution could be implemented, this would be significantly more computationally complex, and would be unnecessary when factoring in the goals of the project.</p>
<p>Currently, the multiple scalar method of this project can allow for an arbitrary number of grain sizes, all modelled as separate scalars; these scalars can be modified to simulate varying grain sizes, dust mass fraction <span class="math inline">\(z\)</span> and bulk densities. This work is expanded upon in section .</p>
<h4 id="propagation-and-simulation">Propagation and simulation</h4>
<p>Hendrix et al. utilise a multi-fluid method of modelling dust in their simulation, having it act as a separate fluid to the stellar wind that is coupled to the wind with the Epstein drag force</p>
<p><span class="math display">\[
f_d = (1-\alpha(T)) \sqrt \frac{8\gamma p}{\pi \rho} \frac{\rho \rho_d}{\rho_p a_d} (\boldsymbol v_d - \boldsymbol v) ~,
\]</span></p>
<p>where <span class="math inline">\(\rho_d\)</span> is the dust fluid density, <span class="math inline">\(\rho_p\)</span> is the grain bulk density, <span class="math inline">\(a_d\)</span> is the grain size and <span class="math inline">\(\alpha\)</span> is the “sticking coefficient” denoting the momentum transfer between dust and gas, defined as</p>
<p><span class="math display">\[
\alpha = 0.35\exp\left( - \sqrt \frac{T T_0}{500~\text K} \right) +0.1 ~,
\]</span></p>
<p>This allows for the accurate modelling of the dynamics of larger grains, which would require significantly more momentum to accelerate; however this is a computationally expensive method due to the modelling of an entirely separate fluid, and calculating a drag force that may be unnecessary considering the conditions of the environment, where the harsh environment would prevent large, heavy dust grains from forming.</p>
<!--  sentence is a little overly long -->
<p>At the moment, this project utilises an advected scalar method, where the dust properties are a conserved quantity of the fluid and propagate alongside it; this is physically accurate for small dust grains, in the order of <span class="math inline">\(10^{-3}\text - 10^{-2}~\mu \text m\)</span> while significantly reduces computational complexity and memory requirements, this can also easily allow the modelling of multiple dust grains by simply iterating the calculations over the number of scalars.</p>
<!-- might make sense to clear this up a little bit, or talk about advected scalars earlier in the project than the next chapter  -->
<!-- is there a better way of doing dashes in latex math? -->
<h4 id="routines-not-included">Routines not included</h4>
<!--  Change wording of this title -->
<p>As the focus of this paper was on the circumstances leading to the spiral structure of the WR98a wind collision region, many dust behaviours were not included or considered, as they would have a negligible impact on the wind structure.</p>
<!--  circumstances? it's? -->
<p>Dust cooling is not included in this project, and is instead used for synthetic observation, in particular synthetic infrared imaging; instead, plasma cooling is the only considered cooling phenomena. While including dust cooling would improve the accuracy of the simulation, the results of the paper indicated that the system was strongly adiabatic, meaning that dust cooling did not need to be considered.</p>
<p>As with cooling, there are no routines for dust destruction, it is assumed that during periods where dust is being created, the amount that is created far exceeds the amount being destroyed; given the violent conditions of a colliding wind binary system and its conduciveness to dust destruction. However, since the dust production rate is calculated from observational data, this would of course include the final amount of dust being produced, which may result in a different distribution of dust throughout the system, but would still be fairly accurate.</p>
<h3 id="overall-comparison-with-project">Overall Comparison With Project</h3>
<h1 id="project-outline">Project Outline</h1>
<h2 id="towards-a-comprehensive-dust-model">Towards a Comprehensive Dust Model</h2>
<h2 id="simulation-of-colliding-wind-binary-systems">Simulation of Colliding Wind Binary Systems</h2>
<h2 id="comparison-with-observational-data">Comparison with Observational Data</h2>
<h1 id="progress">Progress</h1>
<h2 id="dust-modelling">Dust Modelling</h2>
<h3 id="multiple-scalar-model">Multiple Scalar Model</h3>
<h4 id="extensions-to-the-multiple-scalar-model">Extensions to the multiple scalar model </h4>
<h1 id="progression-beyond-the-first-year">Progression Beyond The First Year</h1>
<h1 id="appendix">Appendix</h1>
<h1 id="references" class="unnumbered">References</h1>
<div id="refs" class="references">
<div id="ref-castor_radiation-driven_1975">
<p>Castor, J. I., D. C. Abbott, and R. I. Klein. 1975. “Radiation-Driven Winds in of Stars.” <em>The Astrophysical Journal</em> 195 (January): 157. doi:<a href="https://doi.org/10.1086/153315">10.1086/153315</a>.</p>
</div>
<div id="ref-friend_theory_1986">
<p>Friend, David B., and David C. Abbott. 1986. “The Theory of Radiatively Driven Stellar Winds. III - Wind Models with Finite Disk Correction and Rotation.” <em>The Astrophysical Journal</em> 311 (December): 701. doi:<a href="https://doi.org/10.1086/164809">10.1086/164809</a>.</p>
</div>
<div id="ref-gayley_sudden_1997">
<p>Gayley, K. G., S. P. Owocki, and S. R. Cranmer. 1997. “Sudden Radiative Braking in Colliding Hot-Star Winds.” <em>The Astrophysical Journal</em> 475 (2): 786. doi:<a href="https://doi.org/10.1086/303573">10.1086/303573</a>.</p>
</div>
<div id="ref-hendrix_pinwheels_2016">
<p>Hendrix, Tom, Rony Keppens, Allard Jan van Marle, Peter Camps, Maarten Baes, and Zakaria Meliani. 2016. “Pinwheels in the Sky, with Dust: 3D Modelling of the WolfRayet 98a Environment.” <em>Monthly Notices of the Royal Astronomical Society</em> 460 (4): 3975–91. doi:<a href="https://doi.org/10.1093/mnras/stw1289">10.1093/mnras/stw1289</a>.</p>
</div>
<div id="ref-lamers_introduction_1999">
<p>Lamers, Henny JGLM, and Joseph P Cassinelli. 1999. <em>Introduction to Stellar Winds</em>. Cambridge university press.</p>
</div>
<div id="ref-stevens_stagnation-point_1994">
<p>Stevens, Ian R., and A. M. T. Pollock. 1994. “Stagnation-Point Flow in Colliding-Wind Binary Systems.” <em>Monthly Notices of the Royal Astronomical Society</em> 269 (2): 226–34. doi:<a href="https://doi.org/10.1093/mnras/269.2.226">10.1093/mnras/269.2.226</a>.</p>
</div>
<div id="ref-toro_riemann_2013">
<p>Toro, Eleuterio F. 2013. <em>Riemann Solvers and Numerical Methods for Fluid Dynamics: A Practical Introduction</em>. Springer Science &amp; Business Media.</p>
</div>
<div id="ref-wareing_hydrodynamical_2005">
<p>Wareing, Christopher J. 2005. “Hydrodynamical Modelling of Planetary Nebulae and Their Interaction with the Interstellar Medium.” Ph.D., England: The University of Manchester (United Kingdom).</p>
</div>
<div id="ref-williams_infrared_1987">
<p>Williams, P. M., K. A. van der Hucht, and P. S. The. 1987. “Infrared Photometry of Late-Type Wolf-Rayet Stars.” <em>Astronomy and Astrophysics</em> 182 (August): 91–106.</p>
</div>
<div id="ref-yudin_speckle_2001">
<p>Yudin, B., Y. Balega, T. Bloecker, K.-H. Hofmann, D. Schertl, and G. Weigelt. 2001. “Speckle Interferometry and Radiative Transfer Modelling of the Wolf-Rayet Star WR 118.” <em>Astronomy &amp; Astrophysics</em> 379 (1): 229–34. doi:<a href="https://doi.org/10.1051/0004-6361:20011289">10.1051/0004-6361:20011289</a>.</p>
</div>
</div>
</body>
</html>
