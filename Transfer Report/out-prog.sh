chapters="000-Header.yaml 03-Progress.md"
resizeres=1200x
pandoc -s -S --latex-engine=pdflatex \
--bibliography=./references.bib \
--table-of-contents \
--columns=2 \
--metadata link-citations \
--variable urlcolor=cyan \
-o progress.pdf $chapters #Incorporate all chapters into PDF file
 
echo "Finished PDF rendering"


