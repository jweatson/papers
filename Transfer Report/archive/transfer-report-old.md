---
  title: Transfer Report
  author: Joseph Eatson
  date: May-August 2018
  header: Transfer Report
  geometry: margin=1in
---

# Summary
It is generally agreed upon that the bulk of dust formation in the galaxy occurs inside star systems containing Asymptotic Giant Branch (AGB) stars. However, there are additional formation processes that produce a non-insignificant amount of interstellar dust. One such process is dust production in Wind Collision Regions (WCR's).

Whilst the mechanism behind said dust formation is still poorly understood, the ultimate goal of this project is to provide some initial insight into this process using numerical methods.

<!-- Research mechanisms for dust formation again, verify that the above paragraph is factually correct -->

# Background

## Stellar Winds, Line-Driving Forces

## Wolf-Rayet Stars and Their Beleaguered Partners
Wolf-Rayet stars are extremely luminous stars, with typical luminosities in order order $10^{5-6} L_\odot$ [@crowther2007physical] exceeding the Eddington limit $L_\text{edd} = 1.2 \times 10^{38} \left( \frac{M}{M_\odot}\right) \text{erg/sec}$, causing radiation pressure to become dominant over gravity, leading to the star shedding its envelope, exposing its core.

This, combined with extremely powerful line-driving forces leads to high wind speeds in the order of $10^8 \text{ cm s}^{-1}$ and mass loss rates of $10^{-5} M_\odot \text{ yr}^{-1}$, a truly incredible amount of material being flung from the star.

There are multiple classifications of Wolf-Rayet stars, first there are classifications based on emission lines, as well as early-type and late-type classifications, denoting the age of the star.

Early-type Wolf-Rayet stars are extremely massive from birth, and typically have significantly higher initial masses, while late-type stars are formed when significant hydrogen depletion occurs in extremely massive stars.

Typically massive stars have a binary partner, as massive stars form from clouds with an abundance of stellar material.

<!-- Cite sources stating number of binary partners, I think there is one in the wolf-rayet review paper -->



## How To Cool Something From a Billion Degrees: Shock Cooling

## Binary Systems as Factories for Dust

## It's Taylor Series All the Way Down: Numerical Simulation 101

# Literature Review

## Previous Work

## How Does This Differ From Previous Work?

# Progress Report

## Abstract: How Does This Differ From Other Projects?

## Initial Setup and Issues

## Ray-Tracing

## Dust Destruction Models

## Dust Cooling Models

# Continuation of Work

## Progression to True Fluid Simulation

## Multifluid Dust Simulation

# Conclusions
