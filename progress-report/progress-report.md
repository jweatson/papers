[2]{}

Introduction
============

#### 

#### 

Binary systems where both members are giant stars are extremely bright
sources of X-rays, due to interaction between their stellar winds,
titanic amounts of material is shocked, and heated to extreme
temperatures, exceeding $10^8\text K$. Explaining strong X-ray continuum
emission coming from massive star systems.

#### 

However, despite this harsh environment, cooling in the post-shock flow
is potentially enough to harbour favourable conditions for the formation
of interstellar dust. This could potentially explain further
discrepancies between numerical models and observational results.

#### 

A large portion of my work so far has been setting up and debugging
hydrodynamics code configured to simulate CWB systems, as well as
getting into grips with producing simulations that are stable and
efficient. This paper details these attempts, and offers a brief
background to stellar winds, wind collision and numerical simulation.

Background
==========

Stellar Winds
-------------

#### 

Evidence for the existence of stellar winds has been collected since the
turn of the century. Absorption spectra from bright stars with
calculated Doppler shifts above $10^2 \text{ km/s}^{-1}$ were seen as a
scientific curiosity, and early astrophotography offered proof of
increasing angular diameter of nebulae; by the 1940’s astronomers such
as Underhill found that static models of O-type stars were impossible to
compute, as radiation pressure exceeds the force of gravity on the
outermost layers [@lamers1999introduction]. Finally, observational
evidence was available beyond the optical wavelengths thanks to early
extra-atmospheric telescopes, IR excesses of giant stars were seen as
incontrovertible evidence of large-scale emission of stellar material
from stars, this was backed up with X-ray emissions from massive stars
suggesting strong shocks with the interstellar medium.

#### 

Initially there was some confusion over the density of the outflowing
winds, since the winds were assumed to be driven purely through electron
absorption of photons, the predicted $\dot M$ was several orders of
magnitude lower than observed, however the Castor, Abbott & Klein model
of stellar winds incorporated line driving as well, a significantly
stronger force [@castor1975radiation]. This new model was much more
consistent with the models, and form the basis of all modern numerical
models of winds - albeit with some of the initial assumptions changed or
removed, such as “radial streaming” and the Sobolev approximation
[@pittard1999x].

#### 

While the massively increased luminosity of O-type and Wolf-Rayet stars
is a factor in their significantly denser and faster stellar winds in
comparison to low-mass stars, another contributing factor is the spectra
of massive stars, which emit strongly in the ultraviolet. Resonance
lines in ionised species have opacities multiple orders of magnitude
higher than standard electron scattering, but over a tiny range of
frequencies. As resonance lines would not be efficient in driving
stellar winds on their own, Doppler shift helps with this, due to the
velocity distribution of the plasma as well as the thousands of
resonance lines available for a flow with some degree of metallicity,
line driving becomes significant, combined with momentum coupling in the
plasma this leads to efficient driving of stellar winds [@puls2008mass].

Colliding Wind Binaries
-----------------------

#### 

The Wind Collision Region is governed in the most basic terms by the
wind momentum ratio, $\eta$ of the system:

$$\eta = \frac{\dot M_2 v_{2_\infty}}{\dot M_1 v_{1_\infty}}$$

#### 

Where $\dot M$ is the mass-loss rate of the star and $v_\infty$ is the
wind terminal velocity[^1]. Furthermore using this value, centre of the
wind collision region can be estimated if the separation, $D$ is known:

$$r_{1} = \frac{1}{1+\sqrt \eta} D ,~~~~~ r_{2} = \frac{\sqrt \eta}{1+\sqrt \eta}$$

Where $r$ represents the distance from a star to the WCR. To illustrate
the concept, a series of simulations were generated, and ran up to
$6\times 10^4$ seconds with a variety of values for $\eta$ by varying
the mass loss rate $\dot M$ for one binary member.

\[eta-parameters\]

  Simulation   $\dot M$ ($m_\odot$)   $\eta$   $d_1$ (cm)
  ------------ ---------------------- -------- ------------------------
  1            $2 \times 10^{-7}$     1.00     $8.000 \times 10^{12}$
  2            $4 \times 10^{-7}$     0.50     $9.383\times 10^{12}$
  3            $1 \times 10^{-6}$     0.20     $1.106 \times 10^{13}$
  4            $2 \times 10^{-6}$     0.10     $1.216 \times 10^{13}$
  5            $4 \times 10^{-6}$     0.05     $1.308 \times 10^{13}$
  6            $1 \times 10^{-5}$     0.02     $1.402 \times 10^{13}$

  : Parameters of simulations with varying values for $\eta$

A density plot is generated from these results (figure \[fig:eta\]), it
is clear that the due to the increased wind momentum of the primary
star, the collision region curves, forming a bow shock; as predicted,
the wind collision region centre moves towards the weaker star. Finally
the region facing the stronger star is significantly denser than the
region facing the weaker star, forming a distinct multi-layer
morphology.

![image](images/momentum-ratio/stitch){width="\linewidth"}

#### 

The strongest evidence for the presence of a WCR is a strong X-ray
signature, significantly higher than the typical emission of an OB type
or Wolf-Rayet star. This emission is also highly variable in some
instances due to the orbital motion of the system due to two phenomena,
the revolution of the system modifying the column density along the line
of sight, and in the case of eccentric binary systems, separation
changes the strength of the shocks, changing the X-ray intensity
[@sana2003colliding].

![X-Ray variability of the binary system HD 93403 due to orbital motion
[@rauw2002phase][]{data-label="fig:rauwetal-xraylightcurve"}](images/rauwetal-xraylightcurve.png){width="\linewidth"}

#### 

While the momentum ratio is a useful approximation in the behaviour of
these binary systems, it is fully descriptive of these systems, in the
cases of short binary systems, terminal velocity of the stellar wind may
not be reached before the collision region is reached. Instead the
acceleration of the particles must be considered. Like all problems of
forces this problem can be broken down into discrete components of
force, radiative acceleration and line driving act upon the wind as an
accelerative force from the parent star, and as a “drag” force from the
opposing star. In the case of very compact systems, the gravitational
force of the stars must also be considered, acting as a secondary set of
forces on the stellar winds [@stevens1994stagnation].

#### 

Curiously, despite the exceptionally violent conditions of a colliding
wind binary system, the high density of the post-shock flow results in
strong radiative cooling. This forms the basis of the “dusty pinwheel”
model of colliding wind binaries, prototyped by WR-104
[@tuthill1999dusty]. WR-104 presented a good prototype due to its strong
dust emission, whilst also being observable face on.

#### 

In the case of a ram-unbalanced system, such as an WR+OB system, the
orbital motion, coupled with the strong bow shock generates this
pinwheel effect as seen in figure \[fig:dusty-pinwheel\]. Furthermore
the density of the shock front shields the cooling medium from UV
emission from either binary star. As this outflow is travelling at
escape velocity, the dust is able to escape the system into interstellar
space.

![An example of a “dusty pinwheel” CWB system, pinwheel provides an
effective transport mechanism for dust to escape the system
[@tuthill1999dusty][]{data-label="fig:dusty-pinwheel"}](images/dusty-pinwheel-4x.png){width="\linewidth"}

#### 

If produced in sufficient quantities, binary systems could be an
important producer of interstellar dust in galaxies, leading to the
production of complex space-borne organic molecules.

Interstellar Dust
-----------------

#### 

What makes colliding wind binaries interesting is the presence of
interstellar dust, a precursor and necessary component to complex
organic chemistry in the interstellar medium, as well as the dominant
source of opacity for non-ionising photons in the ISM.

#### 

Dust is composed of carbon compounds and silicates, as well as a
collection of water molecules in the form of ice, held in place through
weak electrostatic forces. Due to sputtering from UV photons and the low
density of the interstellar medium, these grains do not typically grow
beyond a few $\mu \text m$ and

#### 

Grain-surface chemistry is a fascinating subset of astrochemistry, and
is a primary source of complex organics within the interstellar medium
[@herbst2009complex]. There are three main mechanisms for effective
grain-surface chemistry, the Langmuir-Hinshellwood mechanism, the hot
atom mechanism and the Eley-Rideal mechanism.

#### 

By accreting gas, dust grains become surfaces for chemical interaction,
collisions of atoms and gas molecules upon the surfaces of grains causes
them to stick due to the van der Waals potential [@krugel2002physics],
these molecules are bound to the grain, but can move about the surface
by absorbing photons, moving to higher energy levels and settling into a
region with other atoms (Langmuir-Hinshelwood), by directly settling
down into a low energy level and combining with an atom (hot atom), and
by directly settling in a region occupied by other atoms (Eley-Rideal).

![Popular models of grain-surface chemistry
[@herbst2009complex][]{data-label="fig:grain-surface-chemistry"}](images/grain-surface-chemistry){width="\linewidth"}

### Dust Creation and Destruction

#### 

Despite the extremely violent conditions

### Dust Emission

Numerical Simulation
--------------------

#### 

WCR are difficult to observe accurately, with the exception of some rare
cases, due to the small scale of the shock interaction, and the lack of
nearby systems to observe. Whilst being exceptionally bright in the
X-ray and IR regimes, they are simply too distant and small to be
precisely resolved. Additionally, since there are no analytical
solutions to such astrophysical flows, numerical simulation offers a
computationally expensive, but accurate method of simulating these
flows.

![image](images/numerical-simulation){width="\linewidth"}

#### 

The methodology behind numerical simulations relies on breaking the
problem into a large number of cells, where the equations of
hydrodynamics are solved for those regions. Astrophysical flows can be
treated as inviscid; wherein conductivity, viscosity and mass diffusion
can be ignored, hence, they can be solved using the Euler equations
[@pittard1999x]:

$$\frac{\partial \rho }{\partial t} + \nabla \cdot (\rho \boldsymbol{u}) = 0$$

$$\frac{\partial \rho  \boldsymbol{u}}{\partial t} + \nabla \cdot (\rho \boldsymbol{u} u + P) = \rho \boldsymbol{f}$$

$$\frac{\partial \rho \varepsilon}{\partial t} + \nabla \cdot [(\rho \varepsilon + P) \boldsymbol{u}] = \left(  \frac{\rho}{m_H} \right)^2 \Lambda(T) + \rho \boldsymbol{f} \cdot \boldsymbol{u}$$

#### 

These 3 equations represent the continuity equation, the momentum
equation and energy equation in a general form, where
$\varepsilon=\boldsymbol{u}^2/2 + e/\rho$, the total specific energy,
and $e=P/(\gamma -1)$, the internal energy density, and $\gamma$ is the
ratio of specific heats.

#### 

There are multiple algorithms in use, in particular, the piecewise
parabolic method (PPM) and the unsplit piecewise linear method. PPM and
the related PPM with Lagrangian Remap (PPMLR) method utilise another
step that interpolates these grids together prior to integration, using
a high order parabolic function (figure \[fig:ppm\]), this improves
accuracy at the cost of additional compute time.

![Interpolation step of the PPM advection scheme
[@colella1984piecewise][]{data-label="fig:ppm"}](images/ppm){width="\linewidth"}

#### 

The main benefit of this technique is that it is an extremely
parallelisable problem, where groups of cells can be handled by
individual processors. This is accomplished on a larger scale using MPI,
which can delegate groups to other processors on other machines, such as
nodes on a supercomputer, each isolated from another nodes memory pool,
this is referred to as *distributed memory multiprocessing*. Some
overhead is produced by the addition of processors, as additional
communication bandwidth is required to synchronise these groups between
time steps, as well as the overhead resultant from the addition of
“ghost cells” that overlap with other group cells, to keep each group
synchronised during time steps. Another drawback is the increased
instability of the problem, with a large number of cores, dropped
packets or segmentation faults on one machine can cause the entire
simulation to crash, as was the case with earlier simulations ran in
this project.

#### 

As simulations have increased in complexity, as have their memory
requirements; early simulations omitted various features, instead
relying on approximations to save compute time, such as spherical or
cylindrical symmetry to omit dimensions from calculation. However with
modern 3D simulations, the number of cells required runs up against
practical memory limits for high performance computer clusters, a
solution to this is adaptive mesh refinement, or AMR. AMR works by
increasing the effective resolution in regions where additional
complexity is required, each cell dimension is split in half, into a
finer cell, this can extend deeply into (figure
\[fig:numerical-simulation\]). The typical number of levels in a
simulation used in this project is 4, increasing the effective
resolution per axis by a factor of 8 with only a minimal increase in
memory requirements and processing time. This allows for highly complex
simulations typically reserved for HPC clusters to be run on devices
such as workstations or laptops.

#### 

There are some downsides to this method as well, however, large scale
changes to regions can cause AMR to produce errors that propagate
throughout the simulation, these are however fairly insignificant in
most cases compared to the rest of the simulation. Additionally AMR
requires a “join array” that connects cells to their respective partners
in each level, this can rival the size of the numerical grid array in
some instances, but is typically significantly smaller than a higher
resolution grid without AMR.

![image](images/mg/amr){width="\linewidth"}

Literature Review
=================

The Prototype Colliding-Wind Pinwheel WR 104, [@tuthill2008prototype]
---------------------------------------------------------------------

Complex Organic Interstellar Molecules [@herbst2009complex]
-----------------------------------------------------------

Current Progress
================

ARWEN Hydrodynamics Code
------------------------

#### 

In order to get into grips with the concepts surrounding numerical
simulation, a simple hydrodynamical code was provided, ARWEN, which
utilised and PLMDE and PPMLR method.

![A comparison of a colliding wind binary system at various timesteps
using
ARWEN[]{data-label="fig:timecomparison"}](images/arwen/timecomparison){width="\linewidth"}

Whilst this simulation was comparatively easy to set up and run, there
was some difficulty in obtaining the required binaries for MPI on the
provided workstation, this left the simulation running in single
threaded mode for much of the time, hampering progress.

#### 

After this was seen to be running successfully, work begun on utilising
the more advanced mg code, however ARWEN was found to be extremely
useful as a baseline when determining if the outputs from mg were
correct or not, by being consistent with ARWEN’s.

Mg Hydrodynamics Code
---------------------

#### 

Due to the increased complexity of 3D simulations, work was moved from
the provided workstation (4 cores, 16GB RAM) to the ARC3 cluster. In
order to utilise this service I had to attend multiple courses provided
by the HPC department, the courses attended are are in the appendix
section \[attendedcourses\]

#### 

As an AMR hydrodynamics code, mg offers significant benefits in terms of
computing speed and memory usage, making high-resolution 3D simulations
feasible with even a small percentage of the HPC clusters compute power.
This introduces some overhead into the memory and processor
requirements, however it is typically significantly lower than a typical
numerical grid without AMR.

#### 

For instance, in the test to verify the correct operation of mg, $<1\%$
of cells at high grid levels were being utilised, since the WCR is
significantly smaller than the total scale of the simulation. Whilst the
join array, which connects all levels together, takes up a fairly large
amount of memory - $\sim 7\text{GB}$ - total memory usage peaks at
$16.8\text{GB}$, within the range of a standard desktop computer.
Without AMR $\sim 540GB$ of RAM would be required to initialise the
simulation[^2] (figure \[fig:memtest\]).

#### 

In terms of CPU speedup, whilst there is some overhead, and AMR enabled
vs. disabled has not been calculated, due to memory utilisation, there
are an estimated $5011.4\%$ more cells that need to be calculated with
this particular simulation, an addition to computational complexity that
would dwarf any overhead regarding join arrays or refinement.

#### 

Integrating the mg code on a university workstation, as well as the ARC
3 HPC cluster proved to be a significant difficulty, and has taken up
approximately 3 months of work from this project, whilst the majority of
problems have been addressed - detailed in section \[sec:challenges\] of
this paper - there are still some problems with the code regarding
reliability at higher resolutions, beyond $200^3$ coarse cells, as well
as integration of the SILO package, for 3D visualisation and data
storage.

  Level   No. Cells   Memory (MB)         No. Joins   Memory (MB)         Percentage
  ------- ----------- ------------------- ----------- ------------------- ------------
  0       1728000     $7.19\times 10^2$   5472000     $5.69\times 10^2$   100%
  1       13824000    $5.75\times 10^3$   42624000    $4.44\times 10^3$   100%
  2       933368      $3.88\times 10^2$   2943328     $3.12\times 10^2$   0.844%
  3       1168872     $4.86\times 10^2$   3649716     $3.80\times 10^2$   0.132%
  Total   17654240    $7.34\times 10^3$   54689044    $5.70\times 10^3$   

![Final testing to confirm the stability of mg in the CWB case with
orbits enabled, based on simulation orbital parameters, where
$\eta=0.02$
[@pittard20093d][]{data-label="fig:orbittest"}](images/orbittest){width="\linewidth"}

![image](images/orbitsymmetric){width="\linewidth"}

Difficulties and Challenges {#sec:challenges}
---------------------------

#### 

Much of the difficulties for this project have revolved around
compatibility issues with the underlying hydrodynamics code and the
problem at hand, as well as

#### 

The SILO library is used for the exporting of 3D data from mg, the
format offers significant benefits compared to `.xq` output files, they
are significantly more compact, as they preserve the adaptive mesh from
the programme, SILO files can also be loaded into visualisation suits
such as Visit. However, SILO presented significant compatibility
problems when the software was first compiled for the workstation, as
the modules were unable to be sourced or compiled on the workstation,
this was initially suspected to be out-of-date compilers but after
updating there seemed to be another factor at work. Due to the time
taken to update the compilers and the estimated time to assess the
problem, references in mg to the SILO libraries were removed, and
instead a rudimentary plotting script was built using a combination of
python and R (this is covered in further detail in section \[plotting\])

#### 

The major limiting factor of the code was its inability to initialise an
orbiting CWB system without disabling AMR first as AMR cuts the number
of fine cells by a significant factor; coupled with the small scale of
the shock region in comparison to the system itself, much of the
simulation does not need to be processed with fine grids. With AMR
disabled, this drastically increases the amount of memory required as
simulation complexity increases (figure \[fig:memtest\]).

#### 

While a grid could be initialised, a high resolution grid, for instance,
$480^3$ grid cells, such as in would require $\sim 30\%$ of ARC3’s
memory[^3]

![Comparison of memory usage with AMR disabled and enabled with 4 level
depth at various cubic coarse cell values upon initial
refinement[]{data-label="fig:memtest"}](images/mg/memtest){width="\linewidth"}

#### 

Whilst initially debugging the code, trying to work out why memory usage
was so high, the idea that the code would use such a staggeringly large
amount of memory was overlooked, slowing progress. After graphing usage
at low resolutions and extrapolating, this was quickly realised to be
the final major memory problem in the way of the simulation working.
Sections of the code dealing with deleting levels were removed, and the
order of operations was changed in the simulation `.dat` files; by
declaring the Courant number and by enabling orbits after the problem
was declared, this prevented segmentation faults, the reasoning behind
this is still being assessed.

#### 

While the code is now in a working state, there are still a significant
number of instabilities within the code. Increasing the resolution
beyond $200^3$ coarse cells can lead to segmentation faults, despite
memory usage being lower than the maximum allocated in a `qsub` job.
Additionally large scales lead to segmentation faults, which is an issue
for highly elliptical simulations.

Visualisation, Scripts and Other Work {#plotting}
-------------------------------------

#### 

Due to the lack of SILO support so far, a method for visualising
exported data had to be devised. The `.xq` output files from the
`export` command save the data as ASCII

#### 

Despite the large file size due to a lack of adaptive mesh support,
`.xq` files respond extremely well to compression, typically reducing
down to 27% of their initial size using gzip, with further reductions in
small datasets using bzip2, however increased RAM usage of bzip2
prevents it being utilised on large datasets.

#### 

3D visualisation of data is possible, with a 3D file saved to images
that are then combined into a video file using FFMPEG. This output can
show evolution over time, or a full $360^\circ$ rotating view.

#### 

Isosurfaces were used for visualisation of 3D data, as they can be
quickly rendered with R using the package `plot3D`. This produces fairly
attractive results, however, finding the isosurface levels requires
using the `plot` command in mg anyway, and careful adjustment of the
levels is required for good results.

#### 

By assuming the material behaves as an ideal gas, using a combination of
pressure and density data from the outputs can be used to plot the
temperature. Whilst this produces decent results, artefacts in the
pressure data result in incorrect measurements outside of the shock
region. Additionally this routine is fairly memory intensive, requiring
approximately 2GB of memory per instance.

#### 

While the script is extremely fast, it is not particularly flexible, and
designed for iterating through a directory of data from the same
simulation, the next version of these libraries will revolve around a
series of convenient modules and packages to manipulate data, rather
than a “one size fits all” approach.

#### 

The main drawback compared to visit is a lack of interactivity, which
cannot be addressed using this code. Utilising RGL was attempted,
however the memory requirements were too high to save the entirety of
the grid.

#### 

Attempts at converting `.xq` files to FITS files could be an alternate
route to visualisation, if SILO files cannot be written in the future,
since these can be imported into Visit.

#### 

The process was coarsely parallelised by calling multiple processes
using the python `multiprocessing` module, which calls individual R
processes for plotting. This method was selected as it was simplistic,
offering a significant speed-up with minimal programming overhead, as
each process was an individual instance of R and python; however, it is
clearly not efficient, having large memory overhead.

#### 

Progression to Next Year
========================

#### 

Despite an initially slow start due to incompatibilities with the
hydrodynamics code and the computers it was installed on, much of the
technical problems have been resolved. The simulation now correctly
processes orbiting colliding wind binaries at a fairly high resolution
(4 levels at $200^3$ coarse resolution, an equivalent resolution of
$4.096\times 10^9$ cells )

Addition of Features
--------------------

#### 

The major component, is to calculate the formation and destruction of
dust within the simulation. Much of this

#### 

[@herbst2009complex] notes a series of codes that could be used to
estimate grain-surface chemistry, in particular work by Chang, Cuppen
and Herbst, detailing their results of Monte Carlo Chemical reactions,
that could be applied the shock downstream, in order to determine how
the system would affect. This of course, may not have much of a bearing
on the final project, but I believe may be an interesting route to
pursue.

### Dust Formation and Destruction

#### 

Including the simulation of dust formation is a major component of this
project - however, due to the amount of work required to fix issues with
the hydrodynamics code, integration of this has not been considered.

#### 

Before writing the formation and destruction code, cooling needs to be
verified to work, and if not, integrated correctly. Currently the
default values for cooling obey solar-like star laws. However, due to
different abundances of elements - such as increased metallicity and
depleted hydrogen in the case of high mass and Wolf-Rayet stars - new
cooling curves must be acquired from literature and made compatible with
mg.

![Solar cooling curve supplied with mg hydrodynamics
code[]{data-label="fig:coolcurve"}](images/coolcurve){width="\linewidth"}

#### 

Ideally, the code must be lean, compatible with AMR, and running in the
same parallelised environment as the rest of the code. This will take a
large amount of effort and resources to optimise, otherwise it will
become a large-scale bottleneck in the simulation, especially with 3D
simulations near the end of the project.

### mg GPGPU And KNL Rewrite Feasibility

#### 

When running the hydro code on the supercomputer cluster for the first
time, the reliability of acquiring an interactive node for running and
debugging mg without making a job submission (which can take several
hours) was a major setback to getting the code up and running. By
checking the usage across the entire network, it was noted that the 2
KNL nodes in the cluster are seldom used, and considering the benefits
of Xeon Phi’s manycore architecture, tests were run on it instead.

#### 

Xeon Phi is the results of Intel’s Larrabee project, an attempt by the
company to develop a discrete graphics card to compete with Nvidia and
AMD. Shortly afterwards, the company pivoted into making a dedicated
compute processor card, a manycore (typically 64 cores) design with
simplified processors, called Xeon Phi. This originally was not a
particular success, as the coprocessor card required additional
libraries and significant rewriting to leverage this card, combined with
the bandwidth issues of the PCI bus and inferior performance to a Nvidia
compute card.

#### 

Knight’s Landing, the latest iteration of the Xeon Phi line, is mainly
sold in the form of a socketed processor, meaning that it can boot,
compile and run programmes natively across 64 cores; this represents a
drastic improvement in compatibility compared to the predecessors
coprocessor card. This is also coupled with a general performance boost,
at 3. Table \[xeonphi\] details the specification and raw performance of
various socketed processors used in ARC 2 and ARC 3.

#### 

One major advantage of Knight’s Landing is the use of an extremely
large, high speed MCDRAM cache on-die, meaning it can access a
high-bandwidth section of RAM significantly larger than a typical cache
(gigabytes vs. megabytes) This caused a slight speedup in performance
when used in cache mode. The processor also has a mode allowing items in
memory to be assigned explicitly to the MCDRAM cache, this requires a
small degree of rewriting for the processor; a good candidate for this
assignment would be the join array, as it is typically in the order of
10-20GB for large simulations, this could improve performance with
Knight’s Landing.

#### 

However, the major advantage of the Knight’s Landing processor lines is
its support for AVX-512 instruction sets, which allow for vectorisation
and simultaneous execution of multiple mathematical operations on a
large number of values; this is referred to as SIMD, or Single
Instruction, Multiple Data, and increases overall efficiency per clock
cycle. While running with AVX-512 enable and disabled, there was no
measurable performance improvement, suggesting that these instruction
sets were not being utilised correctly. Hence, in order to leverage
additional performance out of the simulations, a rewrite of how the code
stores data structures must be performed, to leverage these instruction
sets.

#### 

A final, more minor, benefit to the KNL architecture is the use of 4-way
hyperthreading, allowing for the use of 256 logical cores, which can
offer a speed-up in the case of some programmes, however this is not the
case with mg, where running at a processor count higher than 64 will
cause drastic slowdown, perhaps due to memory bandwidth limitations
(figure \[fig:optimisation\]). It was further recommended by Chris
Wareing to limit the number of MPI cores to 64 while running, as he had
determined that there was little benefit to hyperthreading with mg
anyway.

#### 

To benchmark this processor, a cylindrically symmetric shock was
simulated at variety of grid resolutions from 1024 to 3880 grid cells
per axis[^4]; the Knight’s Landing Node was then ran with 64 MPI
threads, and 200 threads, and compared with a 12 and 24 core ARC 3 node.

#### 

The KNL node offered significant gains compared to a typical 24 core,
128GB memory node on ARC3, in addition to having markedly faster access
times. Furthermore, the nodes are available for exclusive use nearly all
of the time, meaning that `qrsh` submissions are almost guaranteed,
compared to typical nodes, where it is unlikely that more than 6 cores
can be utilised for interactive mode at any given time. However, the KNL
nodes were bested on low complexity simulations by the 24 core node,
this was perhaps due to memory bandwidth issues, which were partially
resolved by the use of the MCDRAM in the “cache” mode of the processor.

![Time taken for simulation to complete at various grid
resolutions[]{data-label="fig:time-taken"}](images/KNL/time-taken){width="\linewidth"}

#### 

In conclusion, it seems that the KNL node currently can be a replacement
for a single node of the supercomputer with minimal rewrite, but with
the potential for further gains with minor rewrites, compared to CUDA.
If the HPC department decides to include manycore architecture in the
next iteration of ARC, and perhaps include MPI networked
multiprocessing, then the benefits of this form of simulation could be
staggering - however currently, rewriting the code would be gambling on
the intentions of the HPC department, as well as on the effectiveness of
the optimisation.

![2048 coarse grid simulation running at various core counts, indicates
that code is not utilising 4-way hyperthreading
effectively[]{data-label="fig:optimisation"}](images/KNL/thread-optimisation){width="\linewidth"}

  Properties    ARC 2 Node           ARC 3 Node        ARC 3 KNL Node   
  ------------- -------------------- ----------------- ---------------- --
  Processor     Intel E5-2670 (x2)   E5-2650 v4 (x2)   Intel KNL 7230   
  CPU Clock     2.6GHz               2.2GHz            1.3GHz           
  Core Count    16                   24                64               
  Threads       32                   48                256              
  Performance   332 GFLOPS           692 GFLOPS        2662 GFLOPS      

#### 

#### 

GPGPU on the other hand, has an extreme amount of potential, considering
the switch from CPU based multiprocessing, to GPU based processing in
typical supercomputers. All major supercomputers do the bulk of their
work on graphics cards, typically Nvidia cards, leveraging the popular
CUDA compute library. The benefits for moving from CPU to GPU compute
are numerous, namely a massive performance boost for single precision
workloads, and a similar performance boost in high end workstation and
HPC cards, such as the Quadro or Tesla series. This is combined with the
amount of work Nvidia has put into providing documentation for CUDA, and
support for many graphics languages, from Python to Fortran.

#### 

Due to the large-scale rewrite of the code required to leverage CUDA,
and the lack of GPUs in the ARC HPC cluster, it was determined that
rewriting the code for GPGPU support should be delayed until the
specification for ARC 4 is made available, since it is currently
uncertain that more GPU nodes will be included in the next iteration of
ARC.

Modelling of Phenomena
----------------------

Conclusion
==========

Acknowledgements
================

Appendix
========

Courses Attended {#attendedcourses}
----------------

[^1]: Variables with the subscript 1 relate to the stronger stellar
    partner while 2 relates to the weaker partner in the case of an
    asymmetric system.

[^2]: Assuming 4 levels, at $120^3$ coarse cells

[^3]: $480^3$ cells at adepth of 4 levels requires approximately 10TB of
    memory without AMR, while ARC3 as 252 nodes with 128GB of memory
    apiece, a total of 32.256TB

[^4]: 3880 was chosen as a maximum to prevent segmentation faults
