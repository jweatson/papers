#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 18:37:01 2018

@author: joseph
"""

import sys
sys.path.insert(0, '/home/joseph/mgtools/modules/')
import mgplot as mgp
import matplotlib.pyplot as plt

data,header=mgp.readdata("shock1dexample.xq")
pgdata,pgheader=mgp.readdata("shockexamplepg.xq")

rscale=1e12
rhoscale=1e-20
pscale=1e-4
tscale=1e4
uscale=1e8

x=mgp.scale(data[0:,0],rscale)
y=mgp.scale(data[0:,1],rhoscale)
xp=mgp.scale(pgdata[0:,0],rscale)
yp=mgp.scale(pgdata[0:,1],pscale)

fig, ax1 = plt.subplots()
plt.title(r"Shock Interface in Colliding Wind Binary")
ax1.semilogy(x,y, 'b-',label="Density")
ax1.set_xlabel(r"$x$ $(cm)$")
ax1.set_ylabel(r"$\rho$ $g/cm^3$")
ax1.legend(loc=2)
ax2 = ax1.twinx()
ax2.semilogy(xp,yp, 'r--',label="Pressure")
ax2.set_ylabel(r"$P$ $(g/cms^2)$")
ax2.legend(loc=1)
fig.tight_layout()
mgp.savefigure("shockexample.pdf")