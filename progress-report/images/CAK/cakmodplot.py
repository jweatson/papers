#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  8 16:42:45 2018

@author: joseph
"""

import sys
sys.path.insert(0, '/home/joseph/mgtools/modules/')
import mgplot as mgp
import matplotlib.pyplot as plt
import numpy as np

vterm=1
def cak(r):
    return vterm*r**(0.5)
def modcak(r):
    return vterm*r**(0.8)


plt.figure(figsize=(4,4))
r=np.linspace(0,1,2000)
plt.plot(r,cak(r),"b-",label="CAK")
plt.plot(r,modcak(r),"r--",label="Modified CAK")
plt.xlabel(r"$1-R/r$")
plt.ylabel(r"$v/v_\infty$")
plt.legend()
plt.savefig("cakmod.pdf")