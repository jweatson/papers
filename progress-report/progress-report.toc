\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {paragraph}{}{2}{section*.2}
\contentsline {paragraph}{}{2}{section*.3}
\contentsline {paragraph}{}{2}{section*.4}
\contentsline {paragraph}{}{2}{section*.5}
\contentsline {section}{\numberline {2}Background}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Stellar Winds}{2}{subsection.2.1}
\contentsline {paragraph}{}{2}{section*.6}
\contentsline {paragraph}{}{2}{section*.7}
\contentsline {paragraph}{}{2}{section*.8}
\contentsline {paragraph}{}{2}{section*.9}
\contentsline {paragraph}{}{2}{section*.10}
\contentsline {paragraph}{}{2}{section*.11}
\contentsline {paragraph}{}{2}{section*.12}
\contentsline {paragraph}{}{2}{section*.13}
\contentsline {paragraph}{}{3}{section*.14}
\contentsline {paragraph}{}{3}{section*.15}
\contentsline {paragraph}{}{3}{section*.16}
\contentsline {paragraph}{}{3}{section*.18}
\contentsline {paragraph}{}{3}{section*.19}
\contentsline {paragraph}{}{3}{section*.20}
\contentsline {subsection}{\numberline {2.2}Interstellar Dust}{3}{subsection.2.2}
\contentsline {paragraph}{}{3}{section*.22}
\contentsline {paragraph}{}{3}{section*.23}
\contentsline {paragraph}{}{4}{section*.24}
\contentsline {subsubsection}{\numberline {2.2.1}Dust Destruction}{4}{subsubsection.2.2.1}
\contentsline {paragraph}{}{4}{section*.26}
\contentsline {paragraph}{}{4}{section*.27}
\contentsline {paragraph}{}{4}{section*.29}
\contentsline {paragraph}{}{4}{section*.30}
\contentsline {subsubsection}{\numberline {2.2.2}Dust Emission}{4}{subsubsection.2.2.2}
\contentsline {paragraph}{}{4}{section*.31}
\contentsline {subsection}{\numberline {2.3}Colliding Wind Binaries}{4}{subsection.2.3}
\contentsline {paragraph}{}{4}{section*.32}
\contentsline {paragraph}{}{4}{section*.33}
\contentsline {paragraph}{}{5}{section*.34}
\contentsline {paragraph}{}{5}{section*.35}
\contentsline {paragraph}{}{5}{section*.36}
\contentsline {paragraph}{}{5}{section*.39}
\contentsline {paragraph}{}{5}{section*.41}
\contentsline {paragraph}{}{5}{section*.42}
\contentsline {paragraph}{}{6}{section*.44}
\contentsline {paragraph}{}{6}{section*.45}
\contentsline {paragraph}{}{6}{section*.46}
\contentsline {paragraph}{}{7}{section*.48}
\contentsline {subsection}{\numberline {2.4}Strong Shocks and Instabilities}{7}{subsection.2.4}
\contentsline {paragraph}{}{7}{section*.49}
\contentsline {paragraph}{}{7}{section*.51}
\contentsline {paragraph}{}{7}{section*.52}
\contentsline {paragraph}{}{7}{section*.53}
\contentsline {paragraph}{}{7}{section*.54}
\contentsline {paragraph}{}{7}{section*.55}
\contentsline {paragraph}{}{7}{section*.56}
\contentsline {paragraph}{}{7}{section*.57}
\contentsline {paragraph}{}{7}{section*.58}
\contentsline {paragraph}{}{8}{section*.60}
\contentsline {paragraph}{}{8}{section*.61}
\contentsline {subsection}{\numberline {2.5}Numerical Simulation}{8}{subsection.2.5}
\contentsline {paragraph}{}{8}{section*.62}
\contentsline {paragraph}{}{8}{section*.64}
\contentsline {paragraph}{}{8}{section*.65}
\contentsline {paragraph}{}{8}{section*.66}
\contentsline {paragraph}{}{8}{section*.67}
\contentsline {paragraph}{}{8}{section*.68}
\contentsline {paragraph}{}{8}{section*.70}
\contentsline {paragraph}{}{9}{section*.72}
\contentsline {paragraph}{}{9}{section*.73}
\contentsline {paragraph}{}{9}{section*.74}
\contentsline {section}{\numberline {3}Current Progress}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}ARWEN Hydrodynamics Code}{9}{subsection.3.1}
\contentsline {paragraph}{}{9}{section*.76}
\contentsline {paragraph}{}{10}{section*.78}
\contentsline {subsection}{\numberline {3.2}Mg Hydrodynamics Code}{10}{subsection.3.2}
\contentsline {paragraph}{}{10}{section*.79}
\contentsline {paragraph}{}{10}{section*.80}
\contentsline {paragraph}{}{10}{section*.81}
\contentsline {paragraph}{}{10}{section*.82}
\contentsline {paragraph}{}{11}{section*.83}
\contentsline {subsection}{\numberline {3.3}Difficulties and Challenges}{11}{subsection.3.3}
\contentsline {paragraph}{}{11}{section*.87}
\contentsline {subsubsection}{\numberline {3.3.1}Compiling and SILO library issues}{11}{subsubsection.3.3.1}
\contentsline {paragraph}{}{11}{section*.88}
\contentsline {paragraph}{}{11}{section*.89}
\contentsline {paragraph}{}{11}{section*.90}
\contentsline {paragraph}{}{11}{section*.92}
\contentsline {paragraph}{}{12}{section*.93}
\contentsline {paragraph}{}{12}{section*.94}
\contentsline {paragraph}{}{12}{section*.95}
\contentsline {paragraph}{}{12}{section*.97}
\contentsline {paragraph}{}{13}{section*.98}
\contentsline {subsection}{\numberline {3.4}Visualisation, Scripts and Other Work}{13}{subsection.3.4}
\contentsline {paragraph}{}{13}{section*.99}
\contentsline {paragraph}{}{13}{section*.100}
\contentsline {paragraph}{}{13}{section*.101}
\contentsline {paragraph}{}{13}{section*.102}
\contentsline {paragraph}{}{13}{section*.104}
\contentsline {paragraph}{}{13}{section*.105}
\contentsline {paragraph}{}{13}{section*.106}
\contentsline {paragraph}{}{13}{section*.107}
\contentsline {paragraph}{}{13}{section*.108}
\contentsline {paragraph}{}{13}{section*.109}
\contentsline {section}{\numberline {4}Progression to Next Year}{13}{section.4}
\contentsline {paragraph}{}{13}{section*.110}
\contentsline {subsection}{\numberline {4.1}Addition of Features}{13}{subsection.4.1}
\contentsline {paragraph}{}{13}{section*.111}
\contentsline {subsubsection}{\numberline {4.1.1}Dust Formation and Destruction}{14}{subsubsection.4.1.1}
\contentsline {paragraph}{}{14}{section*.112}
\contentsline {paragraph}{}{14}{section*.113}
\contentsline {paragraph}{}{14}{section*.115}
\contentsline {subsubsection}{\numberline {4.1.2}Chemical Reaction Modelling}{14}{subsubsection.4.1.2}
\contentsline {paragraph}{}{14}{section*.116}
\contentsline {subsubsection}{\numberline {4.1.3}Ray-Tracing Code}{14}{subsubsection.4.1.3}
\contentsline {paragraph}{}{14}{section*.117}
\contentsline {paragraph}{}{14}{section*.118}
\contentsline {subsubsection}{\numberline {4.1.4}mg GPGPU And KNL Rewrite Feasibility}{14}{subsubsection.4.1.4}
\contentsline {paragraph}{}{14}{section*.120}
\contentsline {paragraph}{}{14}{section*.121}
\contentsline {paragraph}{}{15}{section*.122}
\contentsline {paragraph}{}{15}{section*.123}
\contentsline {paragraph}{}{15}{section*.124}
\contentsline {paragraph}{}{15}{section*.125}
\contentsline {paragraph}{}{15}{section*.126}
\contentsline {paragraph}{}{15}{section*.127}
\contentsline {paragraph}{}{15}{section*.128}
\contentsline {paragraph}{}{15}{section*.130}
\contentsline {paragraph}{}{16}{section*.133}
\contentsline {paragraph}{}{16}{section*.134}
\contentsline {paragraph}{}{16}{section*.135}
\contentsline {section}{\numberline {5}Conclusion}{16}{section.5}
\contentsline {paragraph}{}{16}{section*.136}
\contentsline {paragraph}{}{16}{section*.137}
\contentsline {paragraph}{}{16}{section*.138}
\contentsline {section}{List of Figures}{17}{section*.138}
\contentsline {section}{References}{17}{section*.139}
\contentsline {section}{\numberline {6}Appendix}{19}{section.6}
\contentsline {subsection}{\numberline {6.1}Courses Attended}{19}{subsection.6.1}
