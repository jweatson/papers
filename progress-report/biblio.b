@article{sana2003colliding,
  title={Colliding Wind Binaries},
  author={Sana, H and Rauw, G},
  journal={Revue des Questions Scientifiques},
  volume={174},
  number={1-2},
  pages={40--48},
  year={2003}
}
